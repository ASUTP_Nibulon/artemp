/*****************************************************************/
/*                        ECL Comfort v1.0                       */
/*                                 by Alexx Grishkov AKA GrayCat */
/*****************************************************************/
/* HD44780 LCD Interface File                                    */
/*****************************************************************/

/*
 *      Uses routines from delay.c
 *      This code will interface to a standard LCD controller
 *      like the Hitachi HD44780. It uses it in 4 bit mode, with
 *      the hardware connected as follows (the standard 14 pin
 *      LCD connector is used):
 *
 *      PORTB bits 4-7 are connected to the LCD data bits 4-7 (high nibble)
 *      PORTB bit 1 is connected to the LCD RS input (register select)
 *      PORTA bit 3 is connected to the LCD EN bit (enable)
 *
 *      To use these routines, set up the port I/O (TRISA, TRISB) then
 *      call lcd_init(), then other routines as required.
 *
 */
 
 /**	
		LCD Timing requirements:
		- Total "E" cycle time: 500ns;
		- "E" pulse time: 230ns;
		- "RS" setup time: 40ns (before "E" front)
		- Data setup time: 80ns (before "E" decay)
		- Command execution: 50us
		- "Clear", "Home" commands: 1.6ms
*/

#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/delay.h>
#include <compat/deprecated.h>
#include <stdlib.h>
#include <string.h>

#include 		"binary_c.h"				// Binary "0B0xxx" consts
#include       "lcd.h"
#define        XTAL_FREQ       14754KHZ            /* Crystal frequency in MHz */
// #include       "delay.h"

LCD_Bank unsigned char LCD_X;      // Location of cursor on LCD : X
LCD_Bank unsigned char LCD_Y;      // Location of cursor on LCD : Y

LCD_Bank char BufChar;
        
#ifdef UseLCDBuf
volatile LCD_Bank unsigned char  LCD_Buf[LCD_Lines*LCD_Chars];    // LCD DDRAM Buffer
#endif

#ifdef UseChars
   /*********** User Character Generator **************/
   /*********** ChGen in EEPROM *************/
__EEPROM_DATA(                /* Symbol 0: "~" */
                00000B,  // Line 0
                01000B,  // Line 1
                10101B,  // Line 2
                00010B,  // Line 3
                01000B,  // Line 4
                10101B,  // Line 5
                00010B,  // Line 6
                00000B); // Line 7

__EEPROM_DATA(                /* Symbol 1: "~" */
                00000B,  // Line 0
                01000B,  // Line 1
                10101B,  // Line 2
                00010B,  // Line 3
                01000B,  // Line 4
                10101B,  // Line 5
                00010B,  // Line 6
                00000B); // Line 7
__EEPROM_DATA(                /* Symbol 2: "�" (gradus) */
                01100B,  // Line 0
                10010B,  // Line 1
                01100B,  // Line 2
                00000B,  // Line 3
                00000B,  // Line 4
                00000B,  // Line 5
                00000B,  // Line 6
                00000B); // Line 7

#endif // UseChars

   /* Here - the address offsets for beginnings of lines */
const unsigned char Y_Offsets[4]={0, 0x40, LCD_Chars, LCD_Chars+0x40};

/* write a byte to the LCD in 4 bit mode */

void lcd_write(unsigned char c)
   {
   unsigned char OldPortVal;
	
   LCD_STROBE_HI;
   OldPortVal = LCD_PORT & (~LCD_Mask_DB);
   LCD_PORT = OldPortVal |  (c & 0xF0);  /* High nibble */
   LCD_STROBE_LO;
   
	LCD_STROBE_HI;
   LCD_PORT = OldPortVal |  (c << 4);    /* Low nibble */
   LCD_STROBE_LO;
   DelayUs(50);
   };

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/** 
	write one character to the LCD -- for "printf"               
*/
void putch(unsigned char c)
   {
           /* Prepare DBx for Output */
        // LCD_TRIS=LCD_TRIS & (~LCD_Mask_DB);

           /* Check for SCROLL need */
        if (LCD_Y==LCD_Lines-1)
           {        /*  Last line  => SCROLL may be needed */
           if ( 	(c=='\n' /* LF */) 
					|| ( (LCD_X==LCD_Chars) && (c!='\r' /* CR */) ) 
					)
					{        /********* OK, Do the Scroll! *************/
									  /* Clear screen */
					cbi( LCD_CTL_PORT, LCD_RS_bit);         /* write Command */
										/* Clear LCD command */
					lcd_write(0x01);    
					DelayMs(4);
					LCD_X=0;
					LCD_Y=LCD_Lines-1;
					};       /********* End of SCROLL ******************/

                        /**** End of SCROLL needed ****/
        };    /* End of  "Check for SCROLL need" */

        switch (c)
           {                 /* Depending on the code:  */
#if 1 // use '\r'
                case '\r':        /* Carriage return */
                          {
                           cbi( LCD_CTL_PORT, LCD_RS_bit);         /* write Command */
                              /* Place cursor to pos.0 on current line */
                           LCD_X=0;
                           BufChar=0x80+Y_Offsets[LCD_Y];
                           lcd_write(BufChar);                            
                          };
                          break;
#endif // Don't use '\r'
                case '\f':        /* Form feed (Clear screen) */
                          {
                                /*   Clear buffer */
                           LCD_X=LCD_Lines*LCD_Chars;
                           cbi( LCD_CTL_PORT, LCD_RS_bit);         /* write Command */
                              /* Clear LCD command */
                           lcd_write(0x01);
                           DelayMs(4);
                           LCD_X=0;
                           LCD_Y=0;
                          };
                        break;

                case '\n':     /* Carriage return + line feed */
                        LCD_X=0;
                        if (LCD_Y<LCD_Lines-1)
                          {       /* Only cursor jump */
                          LCD_Y++;
                                /* Command: CURSOR GOTO */
                          cbi( LCD_CTL_PORT, LCD_RS_bit);         // write Command
                          BufChar=0x80+Y_Offsets[LCD_Y];
                          lcd_write(BufChar);  /* High nibble */
                          };      /* End of Cursor jump */
                        break;

      default:          /* All other chars */

                             /***** Check for LF ****/
                          if ( (LCD_X==LCD_Chars) && (LCD_Y<LCD_Lines-1))
                               {
                                    /* Jump to next line */
                               cbi( LCD_CTL_PORT, LCD_RS_bit);       // write Command
                               LCD_X=0;
                               LCD_Y++;
                                  /* Command: CURSOR GOTO */
                               BufChar=0x80+Y_Offsets[LCD_Y];
                               lcd_write(BufChar);
                               DelayUs(200);
                               };            /* End of LF */
 
                        sbi( LCD_CTL_PORT, LCD_RS_bit);   // write characters
                        // DelayUs(50);
                         
                        lcd_write(c);
                        LCD_X++;        // Increase X
                        break;
            };      /* End of switch (c) */

           /* Return DBx for Input */
        // LCD_TRIS=LCD_TRIS | LCD_Mask_DB;
   };   /* End of putch  */
/*---------------------------------------------------------------------*/

/*++++++++++++++++++++++ lcd_init() +++++++++++++++++++++++++++*/
/* initialise the LCD - put into 4 bit mode */

void lcd_init(void)
   {
#ifdef UseLCDBuf
           /*   Clear buffer */
        BufChar=LCD_Lines*LCD_Chars;
        while (BufChar)  
            LCD_Buf[--BufChar]=' ';
#endif
           /* Prepare all lines for Output */
        // LCD_TRIS=LCD_TRIS & (~LCD_Mask_DB) & (~LCD_Mask_EN) & (~LCD_Mask_RS);

        cbi( LCD_CTL_PORT, LCD_RS_bit);     // write control bytes

        DelayMs(25);    // power on delay
        LCD_PORT = 0x30;   // attention!
        DelayUs(190);
        LCD_STROBE;
        
        DelayMs(10);    // Again 030h => PortB
        LCD_STROBE;
        
        DelayMs(1);
        LCD_PORT = 0x20;   // set 4 bit mode
        DelayUs(190);
        LCD_STROBE;
        
        DelayMs(100);
        lcd_write(0x28);        // 4 bit mode, 1/16 duty, 5x8 font
        
        DelayMs(100);
        lcd_write(0x08);        // display off
        
        DelayMs(100);
        lcd_write(0x01);        // Clear display
        
        DelayMs(4);
        lcd_write(0x0C);        // display on, blink curson off
        lcd_write(0x06);        // entry mode
        
        LCD_X=LCD_Y=0;    // Reset cursor

#ifdef UseChars
            /*********** Transfer CharGen **********/
        LCD_CharSet(/*NumChars*/ 3, /*Offset*/ 0 );
#endif // UseChars

           /* Return DBx for Input */
        // LCD_TRIS=LCD_TRIS | LCD_Mask_DB;
   };


#ifdef UseChars
/*+++++++++++++++++++++++++++ LCD_CharSet() ++++++++++++++++++++++++*/
/* Program CharSet from EEPROM: <NumChars> from <Offset>            */
void LCD_CharSet(char NumChars, char Offset)
   {
       /* Prepare DBx for Output */
   // LCD_TRIS=LCD_TRIS & (~LCD_Mask_DB);

   cbi( LCD_CTL_PORT, LCD_RS_bit);     // write Control bytes
            /*********** Transfer CharGen **********/
   lcd_write(0x40);      // Go to 0th byte in CGRAM
   sbi( LCD_CTL_PORT, LCD_RS_bit);     // write Data bytes

   for (BufChar=0; BufChar < (NumChars*8); BufChar++)
      {
      lcd_write(EEPROM_READ(Offset+BufChar));
      };
      
   cbi( LCD_CTL_PORT, LCD_RS_bit);     // write Control bytes
   lcd_write(0x80);       // Return to DDRAM

    /* Return DBx for Input */
   // LCD_TRIS=LCD_TRIS | LCD_Mask_DB;
   };    /********** End of LCD_CharSet() **********/
/*------------------------------------------------------------------*/
#endif   // UseChars

/*==================================================================*/
