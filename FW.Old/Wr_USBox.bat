@echo off

rem
set Freq=14MHz
rem set Freq=1MHz

if '%1'=='' goto Help
if '%2'=='' goto NoEEP
set EEP=-d %2
goto Burn
:NoEEP
set EEP=
goto Burn

:Burn
echo ==== Burn  code  file %1,
echo ==== Burn EEPROM file %EEP%
echo ==== for AVR Clock = %Freq% ...

D:\!Work\avr\AVReal\avreal32.exe -i4 -o%Freq%  +mega128 -pd="USBox A" -aft2232:reset=acbus2:enable=~acbus3 -e -w -v -c %1 %EEP%

rem  -o14MHz    = XTal frequency
rem -i4         = Logging level: "debug"; default=-i3

exit

:Help
echo ==== Usage:
echo %0 CodeFileName [EEPROMfileName]
