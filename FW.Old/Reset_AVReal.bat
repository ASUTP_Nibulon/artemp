@echo off

rem
set Freq=14MHz
rem set Freq=1MHz

:Burn
echo ==== RESET for AVR Clock = %Freq% ...

D:\!Work\avr\AVReal\avreal32.exe -i4 -o%Freq%  +mega128 -pd="USBox A" -aft2232:reset=acbus2:enable=~acbus3 -b

rem  -o14MHz    = XTal frequency
rem -i4         = Logging level: "debug"; default=-i3

exit

