/** \file
        Firmware for "ARTemp" - Autonomous Registrator of Temperature.
		  Headers.
        ***
        v0.1 by GrayCat
        v0.1 :  25-02-2007
*/

#ifndef ARTemp_h			// Prevent doubling

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                         Global defines:                          ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define NumChannels           8                     	///< Quantity of ThermoChannels
#define VirtChannel           NumChannels 	       	///< Index of Virtual ThermoChannel
#define kol_dat 					30                      ///< Quantity of Sensors per Channel
#define Power_TimeOut			3600u							///< Timeout for Auto-Power-Off
#define NumInSamples				3								///< Number of Input Sampling tries
#define MaxTries						16u							///< Maximum number of read Retries
#define SyncPulse						6u								///< Sync Pulse time, in usec

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                       Physical defines:                          ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define ds_OutPort				PORTC							///< Port for Dallas Outputs
#define ds_InPort					PINA							///< Port for Dallas Inputs Sampling
#define ds_InDir					DDRA							///< Port for Dallas Inputs Direction
#define ds_InForce				PORTA							///< Port for Dallas Inputs Output Force

	// .............. Power:  .......................
#define PowerSw_ON				PORTB |= 1					///< Macro to switch power ON
#define PowerSw_OFF				PORTB &= ~1					///< Macro to switch power OFF
#define PowerPin					(PINB & 1)					///< Macro to Sense power Pin
#define PowerPinIn				{DDRB&=~1; PORTB &=~1;}	///< Macro to switch Power pin to Input
#define PowerPinOut				{ PORTB |=1; DDRB|= 1; }	///< Macro to switch Power pin to Output

#define ChargingState			(PINE & _BV( 7 ) ) 		///< State of Battery Charging
#define ChrgSensePullUp			{ DDRE|=0x40;PORTE|=0x40; }		///< Pull Up Charge Sensor
#define ChrgSenseRelease		DDRE&=~0x40					///< Release Charge Sensor

// PORTE
#define led_line2					2								///< Number of Bit for Charge/Power LEDs
#define led_lineA					4								///< Number of Bit for "A" LEDs common
#define led_lineB					5								///< Number of Bit for "B" LEDs common
#define LEDs_port					PORTF

	// ............... Compatibility: .....................
#define NOP() do { __asm__ __volatile__ ("nop"); } while (0)               
#define WDR() do { __asm__ __volatile__ ("wdr"); } while (0)               
#define SLEEP() do { __asm__ __volatile__ ("sleep"); } while (0)           
#define FALSE 0
#define TRUE 1
// .......... Non-Standard procs:
#define eeprom_rb(addr) eeprom_read_byte ((uint8_t *)(addr))
#define eeprom_rw(addr) eeprom_read_word ((uint16_t *)(addr))
#define eeprom_wb(addr,val) eeprom_write_byte ((uint8_t *)(addr), (uint8_t)(val))

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                            T Y P E S :                           ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

union I1
        {
        uint16_t t;
		  int16_t 	st;		///< Signed 16-bit Temperature
        unsigned char t_c[2];
        };

/// States of Battery: 
typedef enum BattState_e
		{
		bst_Discharged,			///< Discharged, BAD
		bst_Normal,					///< Normal Battery job
		bst_Charging,				///< Battery Charging
		bst_Charged,				///< Battery Charged completely
		} BattState_t;

/// States of Dallas Sensor: 
typedef enum DsState_e
		{
		dst_None,					///< No Dallas @ this address
		dst_OK,						///< Dallas read OK
		dst_NoAns,					///< No answer
		dst_Pending,				///< Now waiting for conversion
		} DsState_t;

/// Flags for general states of application:
typedef enum StatesFlag_e
		{
		f_SearchDone = 1,				///< "Search Finished" flag
		f_PowerOK = 2,					///< "Power Good" flag
		f_ScanDone	= 4,				///< "Scan all sensors Done" flag
		} StatesFlag_t;

/// States of Dallas Channel: 
typedef enum ChState_e
		{
		chst_None		= 0,		///< No Dallases @ this channel
		chst_Configd	= 1,		///< Any Dallases configured for this Channel
		chst_Present	= 2,		///< Any Dallases found on this Channel at Startup
		chst_PhPower	= 4,		///< Phantom power used
		chst_AnyAnswer	= 8,		///< Any Dallas answer encountered in current session
		chst_ShortCq	= 16,		///< Short-cirquit to GND on channel (not rising to "1" after "Reset" pulse)
		chst_ShortCqPw	= 32,		///< Short-cirquit to Power on channel ("1" on idle line)
		} ChState_t;

// ==========================================================================
// ======                      F U N C T I O N S :
// ==========================================================================


void RXproc(void);

// ==========================================================================
#define ARTemp_h			// Prevent doubling
#endif 						// ARTemp_h			// Prevent doubling
// --------------------------------------------------------------------------
