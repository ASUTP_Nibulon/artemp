/** \file
	Firmware for "ARTemp" - Autonomous Registrator of Temperature
	***
	v0.1 by GrayCat
	\date 22-02-2007 : v0.1 : started
	\date 14-07-2007 : v0.2 : Added Oversampling
	\date 08-04-2015 : v0.3 : start correction for 2-wire connection
*/
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <avr/signal.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/delay.h>
#include <compat/deprecated.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

	// ............. AVRlib:  ...................
#include "global.h"             // include our global settings           
#include "rprintf.h"    // include printf function library               

#include "binary_c.h"				// Binary "0B0xxx" consts
#include "1-Wire.h"             	// Local
#include "lcd.h"
#include "UI.h"
#include "Messages.h"
#include "ARTemp_v1.h"				// Application's
#include "BUILDNUM.H_"     // Build Number

// +++++++++++++++++++++++  Global Defines: ++++++++++++++++++++++++++++++++++++
#define MaxErrNum                      4u                      ///< Maximum number of erroneous answers

#define RxBufLen                                256u            ///< Receive buffer length
#define TxBufLen                                256u            ///< Transmit buffer length

//status
#define RX_status_bit   0
#define T0_status_bit   1
#define T1_status_bit   2
#define Srch_status_bit 4


// .......... EEPROM .................
const uint8_t start_eeprom[15] __attribute__ ((section (".eeprom"))) = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const uint8_t start_eeprom2[35] __attribute__ ((section (".eeprom"))) = {};
const uint8_t boad_rate_e __attribute__ ((section (".eeprom"))) = 10; //1
const uint8_t adress_e __attribute__ ((section (".eeprom"))) = 1; ///< Module address(?)
const uint8_t rom_e[NumChannels][kol_dat][8] __attribute__ ((section (".eeprom"))) = {}; //1
//CODE
const unsigned char dscrc_table[256] PROGMEM = {0,94,188,226,97,63,221,131,194,156,126,32,163,253,31,65,
				157,195,33,127,252,162,64,30,95,1,227,189,62,96,130,220,
				35,125,159,193,66,28,254,160,225,191,93,3,128,222,60,98,
				190,224,2,92,223,129,99,61,124,34,192,158,29,67,161,255,
				70,24,250,164,39,121,155,197,132,218,56,102,229,187,89,7,
				219,133,103,57,186,228,6,88,25,71,165,251,120,38,196,154,
				101,59,217,135,4,90,184,230,167,249,27,69,198,152,122,36,
				248,166,68,26,153,199,37,123,58,100,134,216,91,5,231,185,
				140,210,48,110,237,179,81,15,78,16,242,172,47,113,147,205,
				17,79,173,243,112,46,204,146,211,141,111,49,178,236,14,80,
				175,241,19,77,206,144,114,44,109,51,209,143,12,82,176,238,
				50,108,142,208,83,13,239,177,240,174,76,18,145,207,45,115,
				202,148,118,40,171,245,23,73,8,86,180,234,105,55,213,139,
				87,9,235,181,54,104,138,212,149,203,41,119,244,170,72,22,
				233,183,85,11,136,214,52,106,43,117,151,201,74,20,246,168,
				116,42,200,150,21,75,169,247,182,232,10,84,215,137,107,53};
// ================= DRAM Variables:  ======================
uint16_t delay_count = 3686; // freq = 14745600Hz delay_count = freq / 4000
unsigned char adr_h;
unsigned char led_bool;
unsigned char t_hi;
unsigned char t_low;
unsigned char t_hi2;
unsigned char t_low2;
unsigned char dallas[NumChannels+1][kol_dat][2];          ///< Array of current measured temperatures
DsTries_t		ds_NumErr[NumChannels+1][kol_dat];			///< Number of erroneous readings for each Dallas
unsigned char			Chn_Delays[NumChannels+1];				///< Channels' Delays (calculated by Reset Pulse)
ChState_t				Chn_States[NumChannels+1];				///< Channels' Statuses; see #ChState_e 
unsigned char			Chn_CurSen[NumChannels+1];				///< Current quieried Sensor Number for each Channel
unsigned char ROM[8]; // ROM Bit
unsigned char	*ad;										///< Buffer for Address of current Operating Sensor
unsigned char lastDiscrep = 0; // last discrepancy
unsigned char doneFlag = 0; // Done flag
unsigned char FoundROM[kol_dat][8]; // table of found ROM codes
unsigned char numROMs;
unsigned char dowcrc;
unsigned char dowcrc2;
unsigned char cur_dat=0;
unsigned char SrchChan;                                 ///< Channel to be searched for Dallases
unsigned char 				CurTry;									///< Current Reading Try
StatesFlag_t			StatesFlag;    							///< Flags of different states

// .......... Global vars for Channels operation: ...............
uint8_t 					RemapCh;										///< Remapped (physical) number of Channel
uint8_t					ChansMask;									///< Mask for Port operations
uint8_t volatile		*PortPtr;									///< Pointer to Output port of Channel
uint8_t volatile		*InPortPtr;									///< Pointer to Input port of Channel
uint8_t volatile		*PinPtr;										///< Pointer to Input pin of Channel
uint8_t volatile		*DdrPtr;										///< Pointer to DDR register of pin of Channel
/// Macro to turn OFF pull-up on Input channel
#define	CH_PULLUP_OFF()	{ /* input pin to "IN" */ *DdrPtr &= ~(uint8_t)ChansMask; /* No pull-up on input pin */ *InPortPtr &=  ~(uint8_t)ChansMask; }
/// Macro to turn ON pull-up on Input channel
#define	CH_PULLUP_ON()	{ /* Pull-up on input pin */ *InPortPtr |= ChansMask; DelayUs(1); /* input pin to "OUT" */ *DdrPtr |= ChansMask; }

volatile unsigned char RXptr, TXptr, RXlen, TXlen, TXbool;
volatile unsigned char status =0;
volatile unsigned char RXBuffer[RxBufLen];
volatile unsigned char TXBuffer[TxBufLen];
union I1					i1;

	// ............... New: ................
unsigned short		PowerOffTO;						///< PowerOff TimeOut
BattState_t				BattState;						///< Battery state
unsigned char			USBpower;						///< State of Power Source
unsigned char			ShowChan;
unsigned char			ShowSensor;
uint8_t 					ds_Addrs[NumChannels][kol_dat][8];	///< Operating Dallas addresses
DsState_t				ds_Status[NumChannels+1][kol_dat];		///< Dallas Statuses
signed   char			ds_NumOnChan[NumChannels+1];				///< Number of sensors on channel; negative: errors
unsigned char			TotalSensors;
unsigned char			ShowAddr;									///< Display state: showing Dallas Address
unsigned char			InSamples;									///< Input samples accumulator
signed	short			ErrorsAcc;									///< Errors' number accumulator
unsigned short			SessionErrs;								///< Total errors/session accumulator

#define					ShowChStatTO_MAX			52				///< Max. timeout for "Show Channel Status" state
unsigned char			ShowChStatTO;								///< Timeout for "Show Channel Status" state

// test:
// int16_t 					ds_Temps[NumChannels][kol_dat][2];	///< Operating Dallas Temperature

// ====================== Procedure declarations: ========================
void set_boad(unsigned char g);
unsigned char res_dal(  char chanel );
unsigned char write_dal(unsigned char s, unsigned char k);
unsigned char ow_crc( unsigned char x);
unsigned char ow_crc2( unsigned char x);
void write_bit(unsigned char s, unsigned char k);
unsigned char read_bit(unsigned char k);
void FindDevices(unsigned char chanel);
unsigned char First(unsigned char chanel);
unsigned char Next(unsigned char chanel);
void		LEDS_update(void);				///< LEDs procedure:

// ==========================================================================
// ======                      F U N C T I O N S :
// ==========================================================================

void delay(uint8_t ms)
	{
	uint16_t cnt;
	asm volatile
		(
		"\n"
		"L_dl1%=:" "\n\t"
		"mov %A0, %A2" "\n\t"
		"mov %B0, %B2" "\n"
		"L_dl2%=:" "\n\t"
		"sbiw %A0, 1" "\n\t"
		"brne L_dl2%=" "\n\t"
		"dec %1" "\n\t"
		"brne L_dl1%=" "\n\t"
		: "=&w" (cnt)
		: "r" (ms), "r" (delay_count)
		);
	};              // ---------------- delay() -------------------

/// Delay proc.: Real: 4.7 counts per usec
void delay_ns(uint8_t ns)
	{
	uint16_t cnt;
	asm volatile
		(
		"\n"
		"L_dl1%=:" "\n\t"
		"dec %1" "\n\t"
		"brne L_dl1%=" "\n\t"
		: "=&w" (cnt)
		: "r" (ns)
		);
	}

SIGNAL(SIG_OVERFLOW0)
	{
	status |= _BV(T0_status_bit);
	if (KbCnt<0xFF)		// Prevent wrapping,
		KbCnt++;				// update Keyboard counter
	}

SIGNAL(SIG_OVERFLOW1)
	{
	status |= _BV(T1_status_bit);
	}

SIGNAL(SIG_UART1_RECV)
	{
	unsigned char stat_com, resh, resl;
	
	stat_com = UCSR1A;
	resh = UCSR1B;
	resl = UDR1;
	
	
	if ( stat_com & ( (1<<FE)|(1<<DOR) ) ) 		// Receive error?...
		{
		set_boad(eeprom_rb((uint16_t)&boad_rate_e));		// Set Baud rate & Init USART
		return;
		};	
	
	if ((resh & 0x02) !=0)
		{ // start packet
		if (resl == adr_h)
			{
			RXptr = 0;
			RXBuffer[RXptr++] = resl;
			}
		}
		else
			{
			if (RXptr > 1)
				{               // +++++++++++++++++++ Packet Body +++++++++++++++++
				RXBuffer[RXptr] = resl;
				if (RXptr == RXlen)
					{
					RXptr = 0;
					status |= _BV(RX_status_bit);
					}
					else
						if (RXptr<RxBufLen-1)           // Still in Buffer?...
							RXptr++;
							else            // Buffer OverFlow!!!
								RXptr = 0;              // Reset Receiver.
				
				}               // ----------------- Packet Body ---------------------
			if (RXptr == 1)                 // Receiving "Data Length"
				{
				if (resl < RxBufLen-2)          // In-buffer?...
					{               // ++++++++++++++ Place into buffer +++++++++++++++
					RXBuffer[RXptr] = resl;
					RXlen = resl + 1;
					RXptr = 2;
					}               // ------------- Place into buffer ----------------
					else            // Buffer OverFlow!!!
						RXptr = 0;              // Reset Receiver.
				}
			}
	
	};              // ----------------- SIGNAL(SIG_UART0_RECV) ------------------

SIGNAL(SIG_UART1_TRANS)
	{
	unsigned char c;
	
	if (TXptr>=TxBufLen)            // End-of-Buffer reached?...
		{               // ++++++++++++++ Buffer overflow ++++++++++++++++
		TXptr = 0;              // Reset index
		TXbool = 2;             // "Finished" state
		};              // ------------- Buffer overflow -------------
	
	if ((TXbool == 0)||(TXbool == 1))
		{
		c = TXBuffer[TXptr];
		if (TXbool == 1)
			UCSR1B &=0xfe; //set parity low
		if (TXbool == 0)
			TXbool = 1;
		UDR1 = c;
		if (TXptr == TXlen)
			TXbool = 2;
			else
				TXptr++;
		}
	};              // ----------------- SIGNAL(SIG_UART0_TRANS) ----------------

// ==========================================================================
// ========           Now, Regular procedures: 
// ==========================================================================

/// Event handler: Timer0 
void T0proc(void)
	{
	status &= ~_BV(T0_status_bit);
	LEDS_update();	
	}

/// Determine actions with Power
void PowerActions(void)
	{
	// ............. Determine Battery state:  ..................
	if (USBpower)
		{			// +++++++++++++++ USB powered ++++++++++++++++++
		ChrgSenseRelease;		// Release strong PullUp
		NOP();
		if (ChargingState)		// =="1" with weak pull-up?...
			BattState = bst_Normal;
			else
				{		// ++++++++++++++++ Some pull-down present +++++++++++++++++
				ChrgSensePullUp;
				NOP();
				if (ChargingState)		// =="1" with strong pull-up?...
					BattState = bst_Charged;
					else		// Strong pull-down?...
						BattState = bst_Charging;
				};		// --------------- Some pull-down present ---------------
		ChrgSenseRelease;		// Finally, Release strong PullUp
		}			// ----------------- USB powered -----------------------
		else
			{		// +++++++++++++++ Battery Powered +++++++++++++++++
			if ( !(ACSR & _BV(ACO) ) )
				{
				BattState = bst_Normal;
				PORTG |= _BV(3);		// debug out
				}
				else
					{
					BattState = bst_Discharged;
					PORTG &= ~_BV(3);		// debug out
					}
					// Work with Power TimeOut
			if (PowerOffTO)
				PowerOffTO--;
				else
					{
					// char cnt;
					
					PowerSw_OFF;
					TIMSK = 0; 	// Disable Timer0 and Timer1 int.
					cli();
					LEDs_port = 0x00;					// Lit Up all LEDs
					PORTE &= ~_BV(led_line2);				
					PORTE &= ~_BV(led_lineA);
					PORTE &= ~_BV(led_lineB);
					set_sleep_mode(SLEEP_MODE_PWR_DOWN);
					sleep_mode();
					sleep_enable();
					sleep_cpu();
					SLEEP();
					for(;;)
						PowerSw_OFF;
					};
			};		// --------------- Battery Powered -----------------
	
	};		// -------------------- PowerActions() ---------------------


/// Determine validity of given Dallas address in RAM:
/**
	\note WARNING: Desctucts #dowcrc !
		***
	\param *AdrBufVoid - Buffer in RAM with address to test
	\return ==0, if Invalid address; ==1 if valid
*/
char IsValidAddr(void *AdrBufVoid)
	{
	char				*AdrBuf = AdrBufVoid;
	uint8_t			cnt;
	uint8_t			Synd1, Synd2;					// Syndromes for Address validity
	register uint8_t			AdrByte;
		
	dowcrc = 0; // reset the dowcrc
	Synd1 = 0;		// Init Syndrome
	Synd2 = 0xFF;	// Init Syndrome
	for (cnt=0; cnt<8; cnt++)
		{
		AdrByte = *(AdrBuf++);
		ow_crc(AdrByte); // accumulate the CRC
		Synd1 |= AdrByte;			// collect Syndrome
		Synd2 &= AdrByte;			// collect Syndrome		
		};
	
	if ( (0==Synd1) || (0xFF==Synd2) )
		return(0);		//...Bad address
	if ( (dowcrc) && (1 != Synd1) )
		return(0);		//...Bad address
	return(1);		// Good address! :-)
	};		// ------------------- IsValidAddr() ----------------------------

/// Prepare Physical Channel data for 1-Wire transaction
/**
	Remap Channel number; prepare port pointers:
	\param ChanNum - logical Channel number, "0"-based
	\return prepared #PortPtr , #PinPtr , #DdrPtr , #InPortPtr , #ChansMask
*/
void PrepChanPorts(char ChanNum)
	{
	
	RemapCh = ChanNum;
	
	PortPtr = &ds_OutPort;
	PinPtr = &ds_InPort;
	DdrPtr = &ds_InDir;
	InPortPtr = &ds_InForce;
	ChansMask = (uint8_t)1 << ChanNum; // Prepare Mask
	
	};		// ------------------ PrepChanPorts() ------------------------------



/// Perform regular tasks on Flags:
void Reg_Tasks(void)
	{
	if (status & _BV(RX_status_bit) )
		RXproc();
	
	if (status&_BV(T0_status_bit))
		T0proc();
		else
			LEDS_update();
	};		// --------------- Reg_Tasks() ----------------------------

/// Start-up procedure: scan channels, 
/**
	Scan channels, sensors, rewrite constants if needed; get PowerStates.
	\c Attention! Array #ad [] must be filled with valid address!
	\param ChanNum	- number of Channel
	\param SenNum - number of Sensor
*/
void StartScan(uint8_t ChanNum, uint8_t SenNum)
	{
	uint8_t	Trim1, Trim2;
	uint8_t	k;							// Counter
		
	WDR();
	if ( (0==SenNum) ||  (chst_AnyAnswer & Chn_States[ChanNum] ) )	// Presence-pulse returned??...
		for (CurTry=0; CurTry<MaxTries; CurTry++)	
			{		// +++++++ Tries loop +++++++++++++
			// .......... Check valid Answer: ..........
					// ............... Now, ask the sensor: .............
			if ( (0==SenNum) || ( chst_AnyAnswer & Chn_States[ChanNum] ) )		// All OK, or First sensor?...
				res_dal(ChanNum );
				
			
			// Was: res_dal( ChanNum );
			
			Trim1 = write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
			if ( (0==Trim1) || (0xFF==Trim1) )		// Bad value read?...
				continue;									//...try again
				
			for (k=0; k<8; k++)
				write_dal(ad[k], ChanNum); //rom adress
			write_dal( dscmd_ReadScratch, ChanNum);               // Command to Read Scratch
				// ............ Now, Receive from Dallas!: ..........
			dowcrc = 0; // reset the dowcrc
			Trim2 = 0xFF;		// Init "blank detector"
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 1
			Trim2 &= Trim1;		// Detect "blank" answer"
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 2
			Trim2 &= Trim1;		// Detect "blank" answer"
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 3
			Trim2 &= Trim1;		// Detect "blank" answer"
			Reg_Tasks();
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 4
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 5
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 6
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 7
			Reg_Tasks();
			Trim2 &= Trim1;		// Detect "blank" answer"
			ow_crc(Trim1=write_dal(0xff, ChanNum));		// 8
			Trim2 &= Trim1;		// Detect "blank" answer"
			if ( Trim2==0xFF) 		// "blank" answer"?...
				continue;		// ...tries loop
				
			if ( (dowcrc != write_dal(0xff, ChanNum))  )		// final byte
				continue;		// tries loop
			
			ds_Status[ChanNum][SenNum] = dst_Pending;
			Chn_States[ChanNum] |= chst_Present;		// Raise flag
				
				// .............. Read Trim1: .................
			res_dal(  ChanNum );
			write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
			for (k=0; k<8; k++)
				write_dal(ad[k], ChanNum); //rom adress
			write_dal( 0x93, ChanNum);               // Command to Read TRIM1
			Trim1 = write_dal(0xff, ChanNum);					// Read Trim1
				// .............. Read Trim2: .................
			res_dal(  ChanNum );
			write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
			for (k=0; k<8; k++)
				write_dal(ad[k], ChanNum); //rom adress
			write_dal( 0x68, ChanNum);               // Command to Read TRIM2
			Trim2 = write_dal(0xff, ChanNum);					// Read Trim2
				
				// ................ Detect Trims state: ..............
			if ( ( (Trim1 & _0b00000111) != _0b00000101 ) &&  ( Trim2 != _0b10111011 ) )
				{		// ++++++++++++++ Trim values bad, rewrite them! +++++++++++++++
					// .............. Rewrite Trim1: .................
				res_dal(  ChanNum );
				write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
				for (k=0; k<8; k++)
					write_dal(ad[k], ChanNum); //rom adress
				write_dal( 0x95,  ChanNum);               // Command to write TRIM1
				write_dal( 0x9D, ChanNum);						// Write default Trim1
					// .............. Rewrite Trim2: .................
				res_dal(  ChanNum );
				write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
				for (k=0; k<8; k++)
					write_dal(ad[k], ChanNum); //rom adress
				write_dal( 0x63, ChanNum);               // Command to write TRIM2
				write_dal( 0xBB, ChanNum);					// Write default Trim2
				
				Reg_Tasks();
				
					// .............. Apply Trim1: .................
				res_dal( ChanNum );
				write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
				for (k=0; k<8; k++)
					write_dal(ad[k], ChanNum); //rom adress
				write_dal( 0x94, ChanNum);               // Command to apply TRIM1
					// .............. Apply Trim2: .................
				res_dal(  ChanNum );
				write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
				for (k=0; k<8; k++)
					write_dal(ad[k], ChanNum); //rom adress
				write_dal( 0x64, ChanNum);               // Command to apply TRIM2
				};		// --------------- Trim values bad, rewrite them! ---------------
				
			break;		// Finish loop
			};		// -------- Tries loop ----------
	// ds_NumOnChan[ChanNum]++;			// Increase number of found Dallases
	ds_NumErr[ChanNum][SenNum].TryCnt = 0;                    // Reset Errors Number
	};		// ------------------- StartScan() ---------------------------


/// Read current temperature of a sensor and prepare new conversion
/**
	\param ChanNum	- number of Channel
	\param SenNum - number of Sensor
*/
void ReadTemp(uint8_t ChanNum, uint8_t SenNum)
	{
	uint8_t			k;									// Counter

	unsigned char	CurError = dsst_OK;
	unsigned char	t_low, t_hi=0, cr=0, cp=0;
	uint8_t			ReadCRC;				// Value of CRC read from Dallas
	
	if ( (ChanNum>=NumChannels) || (SenNum>=kol_dat) )		// Error in Params?...
		return;
	
		// ..... "Virtual sensor" Detector: .............
	if ( ( VirtChannel+1 == ChanNum )  )	// First sensors on last channel?...
		{		// +++++++++++++ Place into buffer Virtual value ++++++++
		Chn_States[ChanNum] |= chst_AnyAnswer;		// Raise flag on Channel
		if ( 0==SenNum )
			{		// +++++++++ Store Version number +++++++++++++
			dallas[VirtChannel][0][0] = BuildNum_Low ; 		// 
			dallas[VirtChannel][0][1] = BuildNum_High ;		// 1 degree
			ds_NumErr[VirtChannel][0].TryCnt = 0;                    // Reset Errors Number
			// return;
			};		// ---------- Store Version number ------------
		if ( 1==SenNum )
			{		// +++++++++++++ Store Noise level ++++++++++++
			if ( !(StatesFlag & f_PowerOK) && (StatesFlag & f_ScanDone) )		// Power bad?...
				{		// +++++++++++++++ Power ShortCirquit to GND: +++++++++++++
				CurError = dsst_KZ2Gnd;
				goto /* SignalHardError */ SignalError;
				};		// ---------------- Power ShortCirquit to GND -------------
			i1.t = ErrorsAcc;
			dallas[VirtChannel][1][0] = i1.t_c[0] ; 
			dallas[VirtChannel][1][1] = i1.t_c[1] ;
			ds_NumErr[VirtChannel][1].TryCnt = 0;                    // Reset Errors Number
			// return;
			};		// ---------- Store Noise level ---------------
		
		};		// ------------ Place into buffer Virtual value -----------

	ad = ds_Addrs[ChanNum][SenNum] ;											// Pointer to Array of Address-bytes of current sensor
		
	if ( !IsValidAddr(ad) )
		{		// +++++++++++++++ Bad address: +++++++++++++++++++++
		CurError = dsst_BadAddr;
		// Chn_States[ChanNum] &= ~chst_Configd;		// Clear "configured" flag
		goto SignalHardError;
		};		// --------------- Bad address: ---------------------
	Chn_States[ChanNum] |= chst_Configd;		// Raise "configured" flag
	
#if 1
	if ( !(StatesFlag & f_ScanDone) )		// StartUp Scan not finished?...
		{		// +++++++++++++ Continue StartUp Scan  ++++++++++++
		StartScan(ChanNum, SenNum);		// Do scan this slot
		if ( Chn_States[ChanNum] & chst_PhPower)		// Phantom-powered channel?...
			{		// +++++++++++++ Issue a Personal "Convert T" command ++++++++++++++++
			k = res_dal(  ChanNum );
			write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
			for (k=0; k<8; k++)
				write_dal(ad[k], ChanNum); 	// Write ROM address 
			write_dal( dscmd_ConvertT, ChanNum);	
			};		// ------------- Issue a Personal "Convert T" command -----------------		
			
		return;
		};		// ------------ Continue StartUp Scan  -------------
#endif		// def Use_StartScan
		
	if ( ( VirtChannel == ChanNum )  )	// First sensors on last channel?...
		if ( SenNum<2u )		// Virtual sensors?...
			return;
			
	if ( !(StatesFlag & f_PowerOK) )		// Power bad?...
		{		// +++++++++++++++ ShortCirquit to GND: +++++++++++++
		CurError = dsst_KZ2Gnd;
		if (ds_NumErr[ChanNum][SenNum].TryCnt < MaxTries)                // Error limit not reached?...
			ds_NumErr[ChanNum][SenNum].TryCnt++;                                      //...not reached, just increase counter
		goto SignalError;
		};		// ---------------- ShortCirquit to GND -------------
		
		// ............... Now, ask the sensor: .............
	if ( (0==SenNum) || ( chst_AnyAnswer & Chn_States[ChanNum] ) )		// All OK, or First sensor?...
		res_dal( ChanNum );
		
	if ( !( chst_AnyAnswer & Chn_States[ChanNum] ) )	// No Presence-pulse?...
		{		// +++++++++++ Process "No Presence-pulse":
		if ( chst_ShortCq & Chn_States[ChanNum] )
			CurError = dsst_KZ2Gnd;
			else
				if (chst_ShortCqPw & Chn_States[ChanNum] )
					CurError = dsst_KZ2Pwr;
					else
						CurError = dsst_NoAns;
		goto SignalError;
		};		// --------- Process "No Presence-pulse":
	
		// ....... OK, some presence detected!
	t_low = write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
	if ( 0==t_low )
		{		// +++++++++++++++ ShortCirquit to GND: +++++++++++++
		CurError = dsst_KZ2Gnd;
		goto SignalError;
		};		// ---------------- ShortCirquit to GND -------------
	if ( 0xFF ==t_low )
		{		// +++++++++++++++ ShortCirquit to Power: +++++++++++++
		CurError = dsst_KZ2Pwr;
		goto SignalError;
		};		// ---------------- ShortCirquit to Power -------------
	for (k=0; k<8; k++)
		{
		write_dal(ad[k], ChanNum); 	// Write ROM address 
		Reg_Tasks();
		};
	write_dal( dscmd_ReadScratch, ChanNum);               // Command to Read Scratch
		// ............ Now, Receive from Dallas!: ..........
	dowcrc = 0; // reset the dowcrc
	t_low = write_dal(0xff, ChanNum);
	ow_crc(t_low);
	Reg_Tasks();
	t_hi = write_dal(0xff, ChanNum);
	ow_crc(t_hi);
	Reg_Tasks();
	ow_crc(write_dal(0xff, ChanNum));
	Reg_Tasks();
	ow_crc(write_dal(0xff, ChanNum));
	Reg_Tasks();
	ow_crc(write_dal(0xff, ChanNum));
	Reg_Tasks();
	ow_crc(write_dal(0xff, ChanNum));
	Reg_Tasks();
	cr = write_dal(0xff, ChanNum);			// Read conversion coefficient
	ow_crc(cr);
	Reg_Tasks();
	cp = write_dal(0xff, ChanNum);			// Read conversion coefficient
	ow_crc(cp);
	Reg_Tasks();
	
	if ( (t_low==0xFF) && (t_hi==0xFF) && (cr==0xFF) && (cp==0xFF) )
		{
		CurError = dsst_NoAns;
		SessionErrs += ds_NumErr[ChanNum][SenNum].TryCnt;
		goto SignalError;
		};
	ReadCRC = write_dal(0xff, ChanNum);			// Read of Final byte
		
		// ............. Prepare next conversion: .................
	if ( Chn_States[ChanNum] & chst_PhPower)		// Phantom-powered channel?...
		{		// +++++++++++++ Issue a Personal "Convert T" command ++++++++++++++++
		k = res_dal( ChanNum );
		write_dal( dscmd_MatchROM, ChanNum);          // Appeal to given ROM address
		for (k=0; k<8; k++)
			{
			write_dal(ad[k], ChanNum); 	// Write ROM address 
			Reg_Tasks();
			};
		write_dal( dscmd_ConvertT, ChanNum);	
		};		// ------------- Issue a Personal "Convert T" command -----------------
	
	if ( dowcrc==ReadCRC  )		// CRC estimated to Zero?...
		{               // ++++++++++++++++++++ CRC valid +++++++++++++++
		Chn_States[ChanNum] |= chst_AnyAnswer;		// Raise flag on Channel
		CurError = dsst_OK;
		i1.t_c[0] = t_low;		// Store read value into structure
		i1.t_c[1] = t_hi;			// Store read value into structure					
			
		if (ad[0] == 16)        // Family="ds18S20"?...
			{               // ++++++++++++ Family="ds18S20" ++++++++++++++						
			i1.st <<= 3;				// Convert to 4-bits-after-point
			i1.st &= ~0x0F;			// Truncate LSB; got "TEMP_READ"
			i1.st -= 16 / 4;			// Subtract 0.25degrees
			i1.st += cp - cr;			// Add Fractional Part from Counters
			}               // ------------- Family="ds18S20" -------------
		if ( (i1.st < -50*16) || (i1.st > 127*16) )
			{
			CurError = dsst_BadTemp;
			SessionErrs += ds_NumErr[ChanNum][SenNum].TryCnt;
			goto SignalError;
			};
		if ( (85<<4) != i1.st)		// Converted temperature read?...
			{		// ++++++++++++ OK, totally valid temperature! +++++++++++++++
			dallas[ChanNum][SenNum][0] = i1.t_c[0];
			dallas[ChanNum][SenNum][1] = i1.t_c[1];		
			ds_NumErr[ChanNum][SenNum].TryCnt = 0;                    // Reset Errors Number
			ds_NumErr[ChanNum][SenNum].TempValid = 1;                 // Raise flag!
			ds_Status[ChanNum][SenNum] = dst_OK;
			};		// ------------ OK, totally valid temperature! ---------------
		}               // ---------------- CRC valid -------------------------
		else
			{               // +++++++++++++ No Dallas at this point, return -55degrees ++++++++++++++
			CurError = dsst_CRC;
			SessionErrs += ds_NumErr[ChanNum][SenNum].TryCnt;
SignalError:              
			ds_Status[ChanNum][SenNum] = dst_NoAns;
			if (ds_NumErr[ChanNum][SenNum].TryCnt < MaxTries)                // Error limit not reached?...
				{		// +++++++++++++++++++ Non-fatal Error ++++++++++++++
				ds_NumErr[ChanNum][SenNum].TryCnt++;                                      //...not reached, just increase counter
				}		// ------------------- Non-fatal Error ---------------
				else
					{               // ++++++++++++++++++ Error limit reached, return error ++++++++++++
					// ds_NumErr[ChanNum][SenNum].TryCnt = 0;		// Reset tries counter; start over
					ds_NumErr[ChanNum][SenNum].TempValid = 0;                 // Clear "good" flag!
SignalHardError:              
					switch (CurError)
						{
						case dsst_CRC:
								dallas[ChanNum][SenNum][0] = 0x90;		// -55 degrees
								break;
						case dsst_KZ2Gnd:
								dallas[ChanNum][SenNum][0] = 0x80;		// -56 degrees
								break;
						case dsst_KZ2Pwr:
								dallas[ChanNum][SenNum][0] = 0x70;		// -57 degrees
								break;
						case dsst_NoAns:
								dallas[ChanNum][SenNum][0] = 0xB0;		// -53 degrees
								break;
						case dsst_BadAddr:
								dallas[ChanNum][SenNum][0] = 0xA0;		// -54 degrees
								break;
						case dsst_BadTemp:
								dallas[ChanNum][SenNum][0] = 0x60;		// -58 degrees
								break;
						};
					dallas[ChanNum][SenNum][1] = 0xFC;		// -5x degrees										
					};              // --------------- Error limit reached, return error ---------------
			}               // --------------- No Dallas at this point, return -55degrees -------------				
	
	};		// ---------------- ReadTemp() ----------------------------------

/// Make a \c DelayVal -millisecond Delay while performing usual periodic tasks
/**
	\param DelayVal - Value of Delay, in MilliSeconds
*/
void DelayMs_Proc(uint16_t DelayVal)
	{
	uint16_t cnt;
	
	for (cnt=0; cnt<DelayVal*10u; cnt++)
		{		// +++++++++ The Delay loop itself
			// ............ Perform normal tasks:
		WDR();
		Reg_Tasks();
		DelayUs(95);		// the Delay itself
		};		// --------- The Delay loop itself

	};		// ----------- DelayMs_Proc() ------------------
	
/// Main Timer procedure: periodically scan all sensors: (~26 msec period)
void T1proc(void)
	{
	unsigned char ad[8];
	unsigned char k;                        // Channel loop parameter - for broadcast commands
	unsigned char j;                        // Channel loop parameter
	status &= ~_BV(T1_status_bit);
	
	//              TCNT1 = 19455;
	TCNT1 = 64427;																	// Reload timer
	
	if (ShowChStatTO)
		ShowChStatTO--;															// Update timeout
		  
	if (cur_dat == /* Roma's: 0 */ kol_dat )        // _AFTER_ all Dallases read,...
		{                               // ++++++++++++++++++++++ Reset & send "Convert" commands +++++++++++++++++
		led_bool++;
			
		PowerActions();
		
			// Send "Convert" command:
		if (StatesFlag & f_ScanDone)		// Only when StartUp Scan finished
			{		// ++++++++++++++++++ Only when StartUp Scan finished ++++++++++++++++++
			for (j=0; j<NumChannels; j++)
				{		// ++++++++++++++++ All Channels loop ++++++++++++++++
				if ( Chn_States[j] & chst_PhPower)		// Phantom-powered channel?...
					{		// +++++++++++++ Next sensor on Phantom-powered channel +++++++++++++
					ReadTemp(j, Chn_CurSen[j] );
					Chn_CurSen[j]++;		// Advance to next,
					if ( Chn_CurSen[j]==ds_NumOnChan[j])		// Reached end?...
						Chn_CurSen[j] = 0;		//...wrap!
					}		// ------------- Next sensor on Phantom-powered channel --------------
					else
						{		// +++++++++++ Normal-powered channel +++++++++++
						res_dal( j );
						write_dal( dscmd_SkipROM, j);          // Appeal to All ROM addresses
						write_dal( dscmd_ConvertT, j);	
						};		// ------------ Normal-powered channel -----------
				};		// ----------------- All Channels loop --------------
			};		// ----------------- Only when StartUp Scan finished 
		}                       // ---------------- Reset & send "Convert" commands ----------------------

	if (cur_dat<kol_dat)
		{               // +++++++++++++++++ Loop through each Dallas slot on all channels +++++++++++++
		for (j=0; j<NumChannels; j++)
			{ // ++++++++++++++++ All Channels loop ++++++++++++++++

			if ( (0==cur_dat) && !(Chn_States[j] & chst_ShortCq) )
				{
				char		PowerStates;
				res_dal( j );
				DelayUs(500);
				write_dal( dscmd_SkipROM, j);          // Appeal to Any ROM address
				write_dal( dscmd_ReadPower, j);        // Get Power State,
				PowerStates = write_dal(0xFF, j);		// Read Actual Power State,
				if (0 == PowerStates )					// Some Phantom-powered sensors?...
					Chn_States[j] |= chst_PhPower;		//...raise flag!
				if (0xFF == PowerStates )					// No any Phantom-powered sensors?...
					Chn_States[j] &= ~chst_PhPower;		//...clear flag!
				};

			if ( !(Chn_States[j] & chst_PhPower) || !(StatesFlag & f_ScanDone) )		// Normal-powered channel?...
				{		// +++++++++++++++ Next sensor on Normal-powered channel +++++++++++
				WDR();
				ReadTemp(j, cur_dat );
				};
			}               // ----------------- All Channels loop --------------
		}              // ------------------ Loop through each Dallas slot on all channels -----------------
		else
			StatesFlag |= f_ScanDone;			// Raise "Scan Done" flag
								
	if (cur_dat< /* Roma's: 39*/ kol_dat + 900/26 )
		cur_dat++;
		else
			{
			uint16_t		ErrorsAccDecr;
			
			cur_dat = 0;    // From Start
			
				// ......... Errors readout: 256-point low-pass .................	
			ErrorsAcc += SessionErrs;		// Append data byte	
			SessionErrs = 0;			// Reset counter
				// ......... Errors readout: 256-point low-pass .................	
			ErrorsAccDecr = (ErrorsAcc >> 4u);		// Store decrement
			if (ErrorsAcc && (ErrorsAccDecr<4) )		// Too low Decrement?...
				ErrorsAccDecr=4;		// Some decrement
			ErrorsAcc -=  ErrorsAccDecr;				// Fall-down accumulator
			if (ErrorsAcc < 0)
				ErrorsAcc = 0;			// Saturate;
			if (ErrorsAcc > 100*16)
				ErrorsAcc = 100*16;		// Saturate;
			};
			
	PowerPinIn;		// Turn into Input,
	delay_ns(5);	// 4.7 counts per usec => 1usec
	USBpower = (PowerPin) ? 0 : 1;
	PowerPinOut;		// Return to Out
	};              // ------------------ T1proc() ------------------------------

void RXproc(void)
	{
	unsigned char i, k, j, error=0;
	
	status &= ~_BV(RX_status_bit);
	
	dowcrc2 = 0;
	ow_crc2(RXBuffer[0]);
	ow_crc2(RXBuffer[1]);
	for (k = 2; k<= RXBuffer[1]; k++)
			 ow_crc2(RXBuffer[k]);
	if (dowcrc2 == RXBuffer[k])
		{               // +++++++++++++++++++ Packet CRC OK ++++++++++++++++++++++
		switch(RXBuffer[2])
				{
				// poisk datchikov na kanale, rezultat budet v massive FoundROM: 4 - kanal poiska (0..12)
				case 0:
						  if (RXBuffer[3]<NumChannels)
									 {
									 // Roma's: FindDevices(RXBuffer[3]);
									 SrchChan = RXBuffer[3];                         // Store channel number to search
									 status |= _BV(Srch_status_bit); // Flag to call proc.
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = 1;
									 TXBuffer[2] = 255;
									 }
									 else
												error = 1;
						  break;
				// najdennij datchik zanesti v EEPROM: 4 - nomer najdennogo datchika (0..39), 5 - nomer kanala kuda (0..12), 6 - nomer datchika kuda (0..39)
				case 1:
						  if ((RXBuffer[3]<40)&&(RXBuffer[4]<NumChannels)&&(RXBuffer[5]<40))
									 {
									 for (k=0; k<8; k++)
												eeprom_wb((uint16_t)&rom_e+RXBuffer[4]*kol_dat*8+(RXBuffer[5]*8)+k, FoundROM[RXBuffer[3]+1][k]);
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = 1;
									 TXBuffer[2] = 255;
									 }
									 else
												error = 1;
						  break;
				// pokazat najdennij datchik: 4 - nomer datchika (0..39)
				case 2:
						  if (RXBuffer[3]<40)
									 {
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = 8;
									 TXBuffer[2] = FoundROM[RXBuffer[3]+1][0];
									 TXBuffer[3] = FoundROM[RXBuffer[3]+1][1];
									 TXBuffer[4] = FoundROM[RXBuffer[3]+1][2];
									 TXBuffer[5] = FoundROM[RXBuffer[3]+1][3];
									 TXBuffer[6] = FoundROM[RXBuffer[3]+1][4];
									 TXBuffer[7] = FoundROM[RXBuffer[3]+1][5];
									 TXBuffer[8] = FoundROM[RXBuffer[3]+1][6];
									 TXBuffer[9] = FoundROM[RXBuffer[3]+1][7];
									 }
									 else
												error = 1;
						  break;
				// prochitat adresa vseh datchikov v kanale: 4 - nomer kanala (0..12)
				case 3:
						  if (RXBuffer[3]<NumChannels)
									 {
									 TXBuffer[0] = adr_h;
									 // Roma's: TXBuffer[1] = (unsigned char)kol_dat*8;
									 TXBuffer[1] = 0;                // No sensors yet...
									 j = 2;
									 for (i = 0; i< kol_dat; i++)
												{               // ++++++++++ For all Sensors loop ++++++++++++++
												if (j>=TxBufLen-3)              // Buffer overflow?...
														  break;                  //...skip
												if ( 1 || eeprom_rb((uint16_t)&rom_e+RXBuffer[3]*kol_dat*8+i*8) )               // Valid sensor in the Table?...
														  {               // ++++++++++++++++++ Add this sensor to Output +++++++++++++++
														  for (k = 0; k<8; k++)
																	 TXBuffer[j++] = eeprom_rb((uint16_t)&rom_e+RXBuffer[3]*kol_dat*8+i*8+k);
														  };              // -------------- Add this sensor to Output --------------------
												};              // -------------- For all Sensors loop -------------
									 TXBuffer[1] = j-2;              // Real number of bytes
									 }
									 else
												error = 1;
						  break;
				// zapisat adresa vseh datchikov v kanale: 4 - nomer kanala (0..12)
				case 4:
						  if (RXBuffer[3]<NumChannels)
									 {
									 j = 4;
									 for (i = 0; i< kol_dat; i++)
												{               // ++++++++++ For all Sensors loop ++++++++++++++
												if (j>=RxBufLen-1)              // Buffer overflow?...
														  continue;                       //...skip
												if ( 1 || RXBuffer[j] )         // Valid Dallas address?...
														  {               // ++++++++++++++ Store Dallas address ++++++++++++++++++
														  for (k = 0; k<8; k++)
																	 eeprom_wb((uint16_t)&rom_e+RXBuffer[3]*kol_dat*8+(i*8)+k, RXBuffer[j++]);
														  }               // ------------- Store Dallas address ----------------
														  else
																	 break;
												};              // ------------- For all Sensors loop ------------------
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = 1;
									 TXBuffer[2] = 255;
									 }
									 else
												error = 1;
						  break;
				// schitat temperaturu datchika: 4 - nomer kanala (0..12), 5 - nomer datchika (0..39)
				case 5:
						  if ((RXBuffer[3]<NumChannels)&&(RXBuffer[4]<40))
									 {
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = 2;							 
									 TXBuffer[2] = dallas[RXBuffer[3]][RXBuffer[4]][0];
									 TXBuffer[3] = dallas[RXBuffer[3]][RXBuffer[4]][1];
									 }
									 else
												error = 1;
						  break;
				// schitat temperaturu vseh datchikov v kanale: 4 - nomer kanala (0..12)
				case 6:
						  if (RXBuffer[3]<NumChannels)
									 {
									 TXBuffer[0] = adr_h;
									 TXBuffer[1] = (unsigned char)kol_dat*2;
									 j = 2;
									 for (i = 0; i< kol_dat; i++)
												for (k = 0; k<2; k++)
														  {               // ++++++++++ For all Sensors loop ++++++++++++++
														  if (j>=RxBufLen-1)              // Buffer overflow?...
																	 continue;                       //...skip
														  TXBuffer[j++] = dallas[RXBuffer[3]][i][k];
														  };              // -------------- For all Sensors loop ------------
									 }
									 else
												error = 1;
						  break;
				case 7:
						  eeprom_wb((uint16_t)&adress_e, RXBuffer[3]);
						  adr_h = eeprom_rb((uint16_t)&adress_e);
						  break;
				case 8:
						  if ((RXBuffer[3]>4)&&(RXBuffer[3]<11))
									 {
									 eeprom_wb((uint16_t)&boad_rate_e, RXBuffer[3]);
									 set_boad(eeprom_rb((uint16_t)&boad_rate_e));
									 }
									 else
												error = 1;
						  break;
				default:
						  error = 1;
						  break;
				};              // -------------- switch() ---------------
		if ((led_bool & 0x1) !=0)
				cbi(PORTE,led_line2);
				else
						  sbi(PORTE,led_line2);
		led_bool++;
		}               // -------------------- Packet CRC OK --------------------
		else
				{               // ++++++++++++++++ Packet CRC bad ++++++++++++++++++
				return;         // by GC: don't answer on bad CRC
				TXBuffer[0] = adr_h;
				TXBuffer[1] = 1;
				TXBuffer[2] = 253; //oshibka cs pri prieme
				}               // --------------- Packet CRC bad --------------------
	
	if (error == 1)
			 {
			 TXBuffer[0] = adr_h;
			 TXBuffer[1] = 1;
			 TXBuffer[2] = 254; //nepravilnie dannie pri prieme
			 }
	
	TXBuffer[1]++;
	dowcrc2 = 0;
	ow_crc2(TXBuffer[0]);
	ow_crc2(TXBuffer[1]);
	for (k = 2; k<= TXBuffer[1]; k++)
			 ow_crc2(TXBuffer[k]);
	TXBuffer[k] = dowcrc2;
	
	for (k = 0; k< 8; k++)
			 delay_ns(200);
	
	TXptr = 0;
	TXbool = 0;
	TXlen = TXBuffer[1]+1;
	UCSR1B |=0x1; //set parity high
	SIG_UART1_TRANS();
	};              // ----------------- RXproc() -------------------------

void set_boad(unsigned char g)
	{
	UBRR1H = 0;
	switch(g)
		{
		case 5: UBRR1L = 191; break; //4800
		case 6: UBRR1L = 95; break; //9600
		case 7: UBRR1L = 47; break; //19200
		case 8: UBRR1L = 23; break; //38400
		case 9: UBRR1L = 15; break; //57600
		case 10: UBRR1L = 7; break; //115200
		default: UBRR1L = 95; break; //9600
		}
	};              // ------------------- set_boad() -------------------------

void FindDevices(unsigned char chanel)
	{
	unsigned char m, k, ResStat;
	
	ds_NumOnChan[chanel] = numROMs = 0;
	
	ResStat = res_dal(  chanel );
	if( !ResStat ) //Begins when a presence is detected
		{
		if(First(chanel)) //Begins when at least one part is found
			{
			do
				{
				numROMs++;
				for(m=0;m<8;m++)
					{
					FoundROM[numROMs][m]=ROM[m]; //Identifies ROM
					ds_Addrs[chanel][numROMs-1][m] = ROM[m];		// Store in Operating area
					};
				ds_NumOnChan[chanel]++;
				ds_Status[chanel][numROMs-1] = dst_Pending;								// Update sensor status
				TotalSensors++;
				} while (Next(chanel)&&(numROMs<kol_dat)); //Continues until no additional devices are found
			Chn_CurSen[chanel] = numROMs-1;													// Start temp. quering from the end
			}
		}
		else
			ds_NumOnChan[chanel] = /* ResStat */ 0;
			
	// .......... Clear the rest off addresses space:  ..............
	for (m = numROMs+1; m < kol_dat; m++)
		for (k = 0; k<8; k++)
			{
			FoundROM[m][k] = 0;
			ds_Addrs[chanel][m-1][k] = 0;
			ds_Status[chanel][m-1] = dst_None;										// Update sensor status
			};
	
	status &= ~_BV(Srch_status_bit);		// Clear the flag
	
	};                      // ----------------- FindDevices() --------------------------------

unsigned char First(unsigned char chanel)
	{
	lastDiscrep = 0; // reset the rom search last discrepancy global
	doneFlag = FALSE;
	return Next(chanel); // call Next and return its return value
	};              // --------------------- First() -------------------------

unsigned char Next(unsigned char chanel)
	{
	unsigned char m = 1; // ROM Bit index
	unsigned char n = 0; // ROM Byte index
	unsigned char k = 1; // bit mask
	unsigned char x = 0;
	unsigned char discrepMarker = 0; // discrepancy marker
	unsigned char g; // Output bit
	unsigned char nxt; // return value	
	unsigned char TryCnt = 0;		// Tries counter
	int flag;
	
	nxt = FALSE; // set the next flag to false
	do
		{		// +++++++++++++++++ Tries loop ++++++++++++++++++++
		dowcrc = 0; // reset the dowcrc
		
		flag = res_dal( chanel ); // reset the 1-wire
		if (flag)
			ds_NumOnChan[chanel] = flag;
		
		if(flag||doneFlag) // no parts -> return false
			{
			lastDiscrep = 0; // reset the search
			return FALSE;
			}
		write_dal( dscmd_Search, chanel); // send SearchROM command
		do
			{
			x = 0;
			if(read_bit(chanel)==1)
				x = 2;
			
			if(read_bit(chanel)==1)
				x |= 1; // and its complement
			if(x ==3)
				break;
				else
					{
					if(x>0) // all devices coupled have 0 or 1
						g = x>>1; // bit write value for search
						else
							{
							if(m<lastDiscrep)
								g = ((ROM[n]&k)>0);
								else
									g = (m==lastDiscrep);
							if (g==0)
								discrepMarker = m;
							}
					if(g==1)
						ROM[n] |= k;
						else
							ROM[n] &= ~k;
					write_bit(g, chanel); // ROM search write
					m++; // increment bit counter m
					k = k<<1; // and shift the bit mask k
					if(k==0) // if the mask is 0 then go to new ROM
						{ // byte n and reset mask
						ow_crc(ROM[n]); // accumulate the CRC
						n++; k++;
						}
					}
			} while (n<8); //loop until through all ROM bytes 0-7
				 
		if(m<65||dowcrc) // if search was unsuccessful then
			// lastDiscrep=0; // reset the last discrepancy to 0
			continue;		// Next try or exit
			else
				{
				lastDiscrep = discrepMarker;
				doneFlag = (lastDiscrep==0);
				nxt = TRUE; // indicates search is not complete yet, more
				}
		} while( ( (++TryCnt) < 10u ) && (nxt != TRUE) ) ;		// --------------- Tries loop -----------------
	
	if ( m<65 || dowcrc ) // if search was unsuccessful then reset the last discrepancy to 0
		lastDiscrep = 0; // Search failed, no next Dallas
	return nxt;
	};              // ----------------- Next() -----------------------------


/// Send "Reset" pulse to Dallases on given Channels
/**
	Perform "Reset" sequence: pull down for >500usec, wait for 60usec "Presence Pulse" from
	Dallases.
	\param chanel - Index of Channel to operate on
	\return ==0 if any Presence pulse, non-0 ( #ChansMask ) if any Presence detected
*/
unsigned char res_dal( char chanel )
	{
	unsigned char res_stat;
	register unsigned char InSamples;				// Input samples accumulator
	
	PrepChanPorts(chanel);
	res_stat = 100;					// Max. "1"-waiting loop: 60 usec
	InSamples = 0;					// Prepare Input samples accumulator
	
	if ( !(*PinPtr & ChansMask) )			// "Zero" on idle channel?...
		{
		Chn_States[(uint16_t)chanel] |= chst_ShortCq;			// Raise "Short-Cirquited" flag
		return ChansMask;				// No sensors detected!
		};
	
	CH_PULLUP_OFF();   	// Start-of-bit: input pin to "IN"; No pull-up on input pin
	
	*PortPtr |= ChansMask;								// Pull down,
	DelayUs(600);				// Length of "Presence Pulse"

		// .......... Test for KZ to Power:
	if ( (*PinPtr & ChansMask) )			// "1" on pulled-down channel?...
		{
		Chn_States[(uint16_t)chanel] |= chst_ShortCqPw;			// Raise "Short-Cirquited" flag
		return ChansMask;				// No sensors detected!
		};
	
		// ............ Prepare Timer2 for measurement:
	TCNT2 = 0;		// Reset timer,
	TIFR |= _BV(OCF2);			// Clear Overflow flag,
	OCR2 = 100;						// Maximum of 54 usec rise time

	*PortPtr &= ~(uint8_t)ChansMask;							// Release 1-wire
	TCCR2 |= _0b00000010;		// Start Timer!
			//    |||||+++----> Bit 2:0 – CS22:0: Clock Select: 000=Stop; 001=1:1; 010=1:8; 
	
		// ........... Calculare Rise time for Channel:  .............
	for (; !(TIFR & _BV(OCF2)); )		// While limit not reached...
		{
			// ......  Sample pins: .............
		if (*PinPtr & ChansMask)			// Switched to "1"-state?...
			if ( (++InSamples) > 2u)
				{
				TCCR2 &= ~_0b00000010;		// Stop Timer!
				break;
				};
		};
	TCCR2 &= ~_0b00000010;		// Stop Timer!
		
	if ( !(TIFR & _BV(OCF2)) )		// Valid delay value?...
		{
		if (Chn_Delays[(uint16_t)chanel] < TCNT2)
			Chn_Delays[(uint16_t)chanel]++;			// Store delay number
		if (Chn_Delays[(uint16_t)chanel] > TCNT2)
			Chn_Delays[(uint16_t)chanel]--;			// Store delay number
		Chn_States[(uint16_t)chanel] &= ~chst_ShortCq;			// Clear "Short-Cirquited" flag
		}
		else
			{		// +++++++++++++++++ Too long waiting for "1"! +++++++++++++
			Chn_States[(uint16_t)chanel] |= chst_ShortCq;			// Raise "Short-Cirquited" flag
			};		// --------------- Too long waiting for "1"! ----------------
	
		// ..........Finish Reset Pulse: ...........
	CH_PULLUP_ON();   				// Active Pull-Up:  "1" on input pin
	DelayUs(14);                  // Charge capacitors
	CH_PULLUP_OFF();   				// Wait-for-Presence: input pin to "IN"; No pull-up on input pin
	
	DelayUs(50);                                  // Real 30u until Presence Pulse
	res_stat = *PinPtr & ChansMask;                // Sample pins at Presence Pulse, 100 usec after end-of-Reset 

	DelayUs(100);                  
	
	CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
	DelayUs(200);                  // Charge capacitors
	
	if (res_stat)						// No presence-pulse from any sensor?...
		Chn_States[(uint16_t)chanel] &= ~chst_AnyAnswer;			// ...Clear "Presence" flag
		else
			Chn_States[(uint16_t)chanel] |= chst_AnyAnswer;			// ...Set "Presence" flag
	
	return res_stat;
	};              // -------------- res_dal() --------------------

/// Send (and receive) byte to 1-wire bus
/**
        \param s - Byte to write
        \param k - Index of Channel
*/
unsigned char write_dal(unsigned char s, unsigned char k)
	{
	unsigned char i, del2;                                // Bit Loop counter
	unsigned char r=0;
	register unsigned char InSamples;				// Input samples accumulator
	// unsigned char SampleTime;
	
	PrepChanPorts(k);
	
	for (i=0; i<8; i++)
		{
		DelayUs(10);   // 10us - Inter-bit
		del2 = Chn_Delays[k]>>1;			// Prepare Sync-pulse additional width
		InSamples = 0;
		r >>= 1;
		
		CH_PULLUP_OFF();   	// Start-of-bit: input pin to "IN"; No pull-up on input pin
		*PortPtr |=  ChansMask;   // Start-of-bit
		
		DelayUs( SyncPulse );   // 9us - Sync pulse
		_delay_loop_1(del2);		// Additional Sync pulse widening
		del2 = 100;					// Max. "1"-waiting loop: 60 usec
		if (s&0x1)                      // detect Low Bit
			{		// +++++++++ Write "1" == Read +++++++++++++
			
				// ............ Prepare Timer2 for measurement:
			TCNT2 = 0;		// Reset timer,
			TIFR |= _BV(OCF2);			// Clear Overflow flag,
			OCR2 = 100;						// Maximum of 54 usec rise time

			*PortPtr &=  (unsigned char)~ChansMask; // Release line - End-of-Sync
			TCCR2 |= _0b00000010;		// Start Timer!
					//    |||||+++----> Bit 2:0 – CS22:0: Clock Select: 000=Stop; 001=1:1; 010=1:8; 
			
			for (; !(TIFR & _BV(OCF2)); )		// While limit not reached...
				{
					// ......  Sample pins: .............
				if (*PinPtr & ChansMask)			// Switched to "1"-state?...
					if ( (++InSamples) > 2u)
						{
						TCCR2 &= ~_0b00000010;		// Stop Timer!
						CH_PULLUP_ON();   				// Active Pull-Up:  "1" on input pin
						break;
						};
				};
			TCCR2 &= ~_0b00000010;		// Stop Timer!
		
			if ( (InSamples > 2u) && ( TCNT2 < Chn_Delays[k]+20 ) )                // Sampled input OK?...
				{		// ++++++++++++ "1" state read ++++++++++
				r |= 0x80;
				DelayUs(40u-SyncPulse); // after-"1" delay
				}		// ------------ "1" state read ----------
				else		// "0" state read
					{
					if (*PinPtr & ChansMask)			// Already Switched to "1"-state?...
						CH_PULLUP_ON();   				// Active Pull-Up:  "1" on input pin
					DelayUs(2); // after-"0"-read delay
					}
			}		// ------------- Write "1" == Read --------------
			else
				{		// ++++++++++++ Write "0" by master ++++++++++++
				*PortPtr |=  ChansMask; //zapis 0
				DelayUs(60u-SyncPulse); // after-"0"-write delay
				};		// ----------- Write "0" by master -------------
		
		*PortPtr &=  (unsigned char)~ChansMask; // End-of-bit: total 60us
		DelayUs(2);		// 1usec	: Recharge capacitor
		CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
		s >>= 1;					// Next Input bit
		del2 = (Chn_Delays[k]>>1);
		DelayUs(del2);		// 1usec	: Recharge capacitor
		}

	return r;
	};              // ------------------ write_dal() -----------------------

/// Send a single bit to 1-wire bus (for detect procedure)
/**
        \param s - bit to write
        \param k - Index of Channel
*/
void write_bit(unsigned char s, unsigned char k)
	{
	unsigned char del2;                                // Bit Loop counter
	
	PrepChanPorts(k);
	DelayUs(2);   // 10us - Inter-bit
	del2 = Chn_Delays[k]>>1;			// Prepare Sync-pulse additional width
	cli();
	
	CH_PULLUP_OFF();   	// Start-of-bit: input pin to "IN"; No pull-up on input pin
	*PortPtr |=  ChansMask;   // Start-of-bit
	
	DelayUs( SyncPulse );   // 9us - Sync pulse
	_delay_loop_1(del2);		// Additional Sync pulse widening
	if (s)                      // detect Low Bit
		{
		*PortPtr &=  (unsigned char)~ChansMask; //zapis 1 - End-of-Sync
		DelayUs(2);		// 3usec	: Recharge capacitor
		CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
		}
		else
			{
			*PortPtr |=  ChansMask; //zapis 0
			DelayUs(2);		// Compensate for branch mismatch
			};
	DelayUs(45u-SyncPulse); // 
	
	*PortPtr &=  (unsigned char)~ChansMask; // End-of-bit: total 60us
	DelayUs(1);		// 1usec	: Recharge capacitor
	CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
	DelayUs(10);		// 1usec	: Recharge capacitor
	
	sei();
	};              // ------------------- write_bit() ----------------------

/// Read a single bit from 1-wire bus (for detect procedure)
/**
        \param k - Index of Channel
		  \return read bit
*/
unsigned char read_bit(unsigned char k)
	{
	unsigned char r=0, del2, cnt2;
	register unsigned char InSamples;				// Input samples accumulator
	// unsigned char SampleTime;
	
	PrepChanPorts(k);
	
	DelayUs(5);   // 10us - Inter-bit
	del2 = Chn_Delays[k]>>1;			// Prepare Sync-pulse additional width
	InSamples =  0;
	
	cli();
	
	CH_PULLUP_OFF();   	// Start-of-bit: input pin to "IN"; No pull-up on input pin
	*PortPtr |=  ChansMask;   // Start-of-bit
	
	DelayUs( SyncPulse );   // 9us - Sync pulse
	_delay_loop_1(del2);		// Additional Sync pulse widening

#ifdef Use_DebugPin
if (0==k)
	debug_1();
#endif	// def Use_DebugPin
	
		// ............ Prepare Timer2 for measurement:
	TCNT2 = 0;		// Reset timer,
	TIFR |= _BV(OCF2);			// Clear Overflow flag,
	OCR2 = 100;						// Maximum of 54 usec rise time

	*PortPtr &=  (unsigned char)~ChansMask; //zapis 1 - End-of-Sync
	TCCR2 |= _0b00000010;		// Start Timer!
			//    |||||+++----> Bit 2:0 – CS22:0: Clock Select: 000=Stop; 001=1:1; 010=1:8; 
	
		// ........... Calculare Rise time for Channel:  .............
	for (; !(TIFR & _BV(OCF2)); )		// While limit not reached...
		{
			// ......  Sample pins: .............
		if (*PinPtr & ChansMask)			// Switched to "1"-state?...
			if ( (++InSamples) > 2u)
				{
				TCCR2 &= ~_0b00000010;		// Stop Timer!
#ifdef Use_DebugPin
debug_0();
#endif	// def Use_DebugPin
				CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
				break;
				};
		};
	TCCR2 &= ~_0b00000010;		// Stop Timer!
#ifdef Use_DebugPin
debug_0();
#endif	// def Use_DebugPin

	if ( (InSamples > 2u) && ( TCNT2 < Chn_Delays[k]+ 20 ) )                // Sample input
		{
		r = 1;
		del2 = 5;			// prepare after-"1" delay
		}
		else		// "0" state read
			{
			if (*PinPtr & ChansMask)			// Already Switched to "1"-state?...
				CH_PULLUP_ON();   				// Active Pull-Up:  "1" on input pin
			del2 = 1;			// prepare after-"0" delay
			};
	for (cnt2=0; cnt2<del2; cnt2++)
		{		// +++++++++++ Delay 10*5us = 50us ++++++++++
		DelayUs(2); // 
		if ( *PinPtr & ChansMask)                 // Sample input
			CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
		};		// ----------- Delay 10*5us = 50us ------------
	
	*PortPtr &=  (unsigned char)~ChansMask; // End-of-bit: total 60us
	DelayUs(2);		// 1usec	: Recharge capacitor
	CH_PULLUP_ON();   				// End-of-bit: Active Pull-Up:  "1" on input pin
	del2 = 2+Chn_Delays[k];
	DelayUs(del2);		// 1usec	: Recharge capacitor
	
	sei();
	return r;
	};              // --------------------- read_bit() ------------------------


unsigned char ow_crc( unsigned char x)
	{
	dowcrc = pgm_read_byte_near(dscrc_table +(dowcrc^x));
	return dowcrc;
	};              // ----------------- ow_crc() --------------------------

unsigned char ow_crc2( unsigned char x)
	{
	dowcrc2 = pgm_read_byte_near(dscrc_table +(dowcrc2^x));
	return dowcrc2;
	};              // -----------------  ow_crc2() ----------------------

// ................... 
/// LEDs procedure:
void		LEDS_update(void)
	{
	unsigned char		cnt;
	unsigned char		ChanMask;
	
	LEDs_port = 0xFF;					// Put down all LEDs
	PORTE |= _BV(led_line2);				
	PORTE ^= _BV(led_lineA);
	PORTE ^= _BV(led_lineB);
	
	if ( PORTE & _BV(led_lineB) )
		{		// +++++++++++++++++ RED LEDs +++++++++++++++
		LEDs_port = ~_BV( ShowChan );
		
			// ............. Display Errors: ............
		ChanMask = 1;
		for (cnt=0; cnt<NumChannels; cnt++)
			{
			if ( ds_NumOnChan[cnt] < 0)		// Error on given Channel?...
				LEDs_port &= (u08) ~ChanMask;		// Switch On LED
			ChanMask <<= 1;
			};
		
			// ............. Display Battery state:  .................
		if ( 		(bst_Charging==BattState) 
				|| ( (bst_Discharged==BattState) && (led_bool & 1) )
			)
			{		// +++++++++++++++++ Need4Batt. display Red ++++++++++++++++++++++
			PORTE &= ~_BV(led_line2);		// Turn On  LED
			};		// -------------- Need4Batt. display Red -------------------------
		}		// -------------- RED LEDs -----------------
		else
			{		// ++++++++++++++++++ Green LEDs  +++++++++++++++++++++++
			LEDs_port = ~_BV( ShowChan );
			
				// ............. Display Errors: ............
			ChanMask = 1;
			for (cnt=0; cnt<NumChannels; cnt++)
				{
				if ( ds_NumOnChan[cnt] > 0)		// Sensors on given Channel?...
					LEDs_port &= (u08) ~ChanMask;		// Switch On LED
				ChanMask <<= 1;
				};
			
				// ............. Display Battery state:  .................
			if ( 		(bst_Charging==BattState) 
					|| (bst_Normal==BattState) 
					|| (bst_Charged==BattState) 
				)
				{		// +++++++++++++++++ Need4Batt. display Red ++++++++++++++++++++++
				PORTE &= ~_BV(led_line2);		// Turn On  LED
				};		// -------------- Need4Batt. display Red -------------------------
			};		// ------------------- Green LEDs -----------------------
		
		// .............. Check Keys:  ......................
	if (KeyPressed() )
		{		// +++++++++++++++++ Some key pressed ++++++++++++++++++++++
		ShowChStatTO = 0;
		switch (KeyStat)
			{
			case kb_Up: 
					ShowSensor++;
					if (ShowSensor >= ds_NumOnChan[ShowChan] )		// Overflow?...
						ShowSensor=0;		//...wrap!
					ShowSensor %= kol_dat;
					break;
			case kb_Down: ///< "Down" key
					ShowSensor += kol_dat-1;
					ShowSensor %= kol_dat;
					if (ShowSensor >= ds_NumOnChan[ShowChan] )		// Overflow?...
						ShowSensor=ds_NumOnChan[ShowChan]-1;		//...wrap!
					break;
			case kb_Left:    ///< "Left" key
					ShowChan += NumChannels-1;
					ShowChan %= NumChannels;
					ShowChStatTO = ShowChStatTO_MAX;
					break;
			case kb_Right:  ///< "Right" key
					ShowChan++;
					ShowChan %= NumChannels;
					ShowChStatTO = ShowChStatTO_MAX;
					break;
			case kb_Enter:	///< "Enter" key
					ShowAddr ^= 1;			// Toggle show status
					break;
			case kb_Esc:	///< "Escape" key
					ShowSensor = 0;			// Reset 
					break;
			default:
					break;
			};		// -------------- switch (KeyStat) -----------------------
		};		// ------------------ Some key pressed ----------------------
	};		// --------------- LEDS_update() ------------------------------
// --------------------------------------------------------------------------

// =================================================================
/// .................. MAIN Function:  .................
int main(void)
	{
	DDRA = 0;					// Dallas Inputs
	DDRB = 0xFD;					// Power; Buttons
	DDRC = 0xff;				// Dallas Outputs
	DDRD = 0xFF & ~0x04; // LCD Data; I2C; UART
	DDRE = 0x04 | 0x30 ; 			// Programming; LEDs, Sensors
	DDRF = 0xFF;				// LEDs array
	DDRG = 0xFF;				// LCD controls
	
// debug:
// StatesFlag |= f_ScanDone;			// Raise "Scan Done" flag
StatesFlag |= f_PowerOK;
	
	if ( MCUCSR & _BV(BORF) )
		{		// +++++++++++++++++ Was restarted after BOD... +++++++++++++++
		PowerSw_OFF;
		TIMSK = 0; 	// Disable Timer0 and Timer1 int.
		cli();
		LEDs_port = 0x00;					// Lit Up all LEDs
		PORTE &= ~_BV(led_line2);				
		PORTE &= ~_BV(led_lineA);
		PORTE &= ~_BV(led_lineB);
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		sleep_mode();
		sleep_enable();
		sleep_cpu();
		SLEEP();
		for(;;)
			PowerSw_OFF;
		};		// ----------------- Was restarted after BOD... ---------------
	
	PowerPinOut;
	PowerSw_ON;					// Power On immediatelly
	PowerOffTO = Power_TimeOut;			// Init Time-out timer
		
	set_boad(eeprom_rb((uint16_t)&boad_rate_e));
	adr_h = eeprom_rb((uint16_t)&adress_e);
	
	UCSR1B = _BV(RXCIE1)|_BV(TXCIE1)|_BV(RXEN1)|_BV(TXEN1)|_BV(UCSZ12); //9-bit
	UCSR1C = 6;
	TCCR0 = 5; //T0, Ck/128 14745600 Hz /128/256 =  450 Hz
	TCCR1A = 0; //Normal mode Timer1 without compare and PWM
	TCCR1B = 4; //T1, 14.7456 Mhz Ck/256 =  57600Hz, overflow = 46080 / 57600  = 0.8 sec

		// ........... SetUp Timer2, for Dallas-bit delays:
		// Clock = 14.7456M/8 = 1843200Hz = 0.54usec
	TCCR2 = _0b00000010;
			//   ||||||||
			//   |||||+++----> Bit 2:0 – CS22:0: Clock Select: 000=Stop; 001=1:1; 010=1:8; 011=1:64; 100=1:256
			//   ||++~-------> Bit 5:4 – COM21:0: Compare Match Output Mode: 00="Normal port operation, OC2 disconnected"
			//   |+--+-------> Bit 6, 3 – WGM21:0: Waveform Generation Mode: 00=Normal,01=PWM phase-correct,10=CTC,11=Fast PWM
			//   +-----------> Bit 7 – FOC2: Force Output Compare
	
	TIMSK = 5; //Enable Timer0 and Timer1
	// ................ Comparator:  .................
	SFIOR &= ~_BV(ACME);		// Multiplexer off,
	ACSR = _0b01000000;
	//        ||||||||
	//        |||||||+-->	ACIS0		: \_ Analog Comparator Interrupt Mode Select
	//        ||||||+--->	ACIS1		: / 
	//        |||||+---->	ACIC		: Analog Comparator Input Capture Enable
	//        ||||+----->	ACIE		: Interrupt Enable
	//        |||+------>	ACI		: Interrupt flag
	//        ||+------->	ACO		: Comparator Output
	//        |+-------->	ACBG		: Compare with BandGap, i.e. Internal Vref
	//        +--------->	ACD		: Comparator Disable
	
	RXptr = 0; TXptr = 0;
	PORTA = 0xFF;           // PullUps on Inputs
	PORTG = 0;
	PORTD = 0;
	PORTF = 0;
	sbi(PORTE,led_line2);
	PORTE |= _BV(led_lineA);
	PORTE &= ~_BV(led_lineB);
	status = 0;
	
	lcd_init();
	rprintfInit( putch );
	rprintf(msg_Hello, BuildNum_ch2+'0', BuildNum_ch1+'0', BuildNum_ch0+'0');
	DelayMs( 250 );
	
		// .............. Initial search for devices, into RAM: .............
	TotalSensors = 0;
	for (ShowChan=0; ShowChan<NumChannels; ShowChan++)
		{
		DelayMs( 10 );
		LEDS_update();
		rprintf( "\fScan ch.%d\n", ShowChan );
		DelayMs( 150 );

		FindDevices(ShowChan);
		};
	ShowChan = 0;
	rprintf( msg_TotSens, TotalSensors);
	DelayMs( 250 );
	
	/*
	for (j=0; j<2; j++)
			 for (k=0; k<8; k++) eeprom_wb((uint16_t)&rom_e+2*kol_dat*8+(j*8)+k, FoundROM[j+1][k]);*/
	
	
	for (;;)
		{               // ++++++++++++++++++ Main Program Loop +++++++++++++++++++
		sei();
		if (status & _BV(RX_status_bit) )
				RXproc();
		if (status & _BV(Srch_status_bit) )
				FindDevices( SrchChan);
		if (status&_BV(T0_status_bit))
				T0proc();
		if (status&_BV(T1_status_bit))
				T1proc();					
		
		if ( 0 == ( (cur_dat+1) & 15)  )
			{		// ++++++++++++++ Update display: ++++++++++++++++
			union I1				chanT;
			
			if ( ShowChStatTO )
				{
				rprintf( msg_ChanData, ShowChan+1, ds_NumOnChan[ShowChan], Chn_Delays[ShowChan]   );
				if (ds_NumOnChan[ShowChan] >0 )
					rprintf( msg_ConnType, ( (Chn_States[ShowChan] & chst_PhPower) ? 2 : 3 )  );
				}
				else
					{		// ++++++++++++++++++ Print value of selected sensor:
					rprintf(msg_Query, ShowChan+1, ShowSensor+1 );
					if (ds_NumOnChan[ShowChan] >0 )
						{		// ++++++++++++++ Sensors present on Channel +++++++++++++++
						if ( dst_OK == ds_Status[ShowChan][ShowSensor])
							{		// +++++++++++++++++++ This sensor status "OK", show its temperature
							chanT.t_c[0] = dallas[ShowChan][ShowSensor][0];
							chanT.t_c[1] = dallas[ShowChan][ShowSensor][1];
							if (ShowAddr)		/* addr[0] == High Byte = Family */
								{		// ++++++++++++++ Show addr +++++++++++++++
								unsigned char cnt, curaddr;
								
								rprintf(" A\n");
								for (cnt=0; cnt<6; cnt++)
									{
									curaddr = ds_Addrs[ShowChan][ShowSensor][2+cnt];		// Get address,
									rprintf("%02x", curaddr ); 
									};
								}		// -------------- Show addr --------------------
								else
									rprintf("\nt=%d.%d", chanT.st/16, ((chanT.st & 0x0F)*10)/16 );
							}		// ------------------------ This sensor status "OK", show its temperature
							else
								if ( dst_Pending == ds_Status[ShowChan][ShowSensor])
									rprintf( msg_Pending, Chn_CurSen[ShowChan] );
									else
										rprintf( msg_NoSens );
						}		// ------------ Sensors present on Channel ----------------
						else
							{		// ++++++++++++++ No Sensors on Channel! ++++++++++++++
							switch ( ds_NumOnChan[ShowChan])
									{		// ++++++++++ Type of Malfunction on Channel:  .............
									case chst_Empty:
										rprintf( msg_NoSenss );
										break;
									case chst_KZ2Gnd:
										rprintf( msg_KZ2Gnd );
										break;
									case chst_KZ2Pwr:
										rprintf( msg_KZ2Pwr );
										break;
									};		// -------------- Type of Malfunction on Channel: -----------
							};		// -------------- No Sensors on Channel! --------------
					};		// ------------------- Print value of selected sensor:

					
			};		// -------------- Update display: ---------------------

		}               // ------------------ Main Program Loop --------------------

	return 1;
	};              // ------------------------ MAIN () ------------------------------------
// --------------------------------------------------------------------------
