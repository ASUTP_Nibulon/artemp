/*
 *	LCD interface header file
 *	See lcd.c for more info
 */

#ifndef LCD_h

/************************** General LCD Spec. *****************************/

/* !!! Here, specify whether you want to use LCD Buffer for scrolling !!! */
// #define UseLCDBuf

// =============== PLATFORM-SPECIFIC:  ==============================
#define LCD_Bank		///< RAM Bank assignment for LCD Vars
// .......... Hardware bit-ops for PIC:  .............
// #define sbi( PORT, bitn) {PORT##bitn = 1;}
// #define cbi( PORT, bitn) {PORT##bitn = 0;}

// .......... Hardware defines for AVR:  .............
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)               
#define DelayMs(x) _delay_ms(x)
#define DelayUs(x) _delay_us(x)

// Specify whether User Characters are used: 
// #define UseChars	1		///< User Characters are used

/// ================= LCD Size:  ===========================
// #define LCD16x2                  ///< LCD Type
// #define LCD16x1                  ///< LCD Type
// #define LCD16x1x2                  ///< LCD Type: 16-char line represented by 2-line videoRAM
// #define LCD10x4                  ///< LCD Type
#define LCD12x2                  ///< LCD Type

/*------------------------------------------------------------------------*/

/* write a byte to the LCD in 4 bit mode */
extern void lcd_write(unsigned char);
	
/* intialize the LCD - call before anything else */
extern void lcd_init(void);

extern void putch(unsigned char);   /* for "printf" */

/*	Set the cursor position */
#define	lcd_cursor(x)	lcd_write(((x)&0x7F)|0x80)

/*+++++++++++++++++++++ SetCursor() +++++++++++++++++++++++++++++++*/
/* Sets cursor to X=XY[5..0] : Y=XY[7..6], 
   State[0]: BLOCK cursor, State[1]: Enable Cursor */
extern void SetCursor(unsigned char ,unsigned  char );

/*+++++++++++++++++++++++++++ LCD_CharSet() ++++++++++++++++++++++++*/
/* Program CharSet from EEPROM: <NumChars> from <Offset>            */
extern void LCD_CharSet(char NumChars, char Offset);

/*********************************************************************/
/*      Physical connection:                                         */
/*********************************************************************/

#define LCD_PORT   	PORTD          ///< Port for LCD Data Lines
#define LCD_CTL_PORT PORTG          ///< Port for LCD Control Lines
#define LCD_EN_bit   1            	///< Enable/Clock
#define LCD_RS_bit   0            	///< Register Select

#define LCD_Mask_EN (unsigned char) (1<<LCD_EN_bit)    // Mask of "E" line - for "PORTx=" output
#define LCD_Mask_RS (unsigned char) (1<<LCD_RS_bit)    // Mask of "RS" line - for "PORTx=" output
#define LCD_Mask_DB (unsigned char) 0b11110000    // Mask of "DBx" lines - for "PORTx=" output

#define LCD_TRIS_RS        DDRG            	///< Configure I/O for Enable/Clock
#define LCD_TRIS_RW        DDRG            	///< Configure I/O for Read / ~Write
#define LCD_TRIS           DDRB             	///< Configure I/O for Data


/// LCD strobe HI action:
#define LCD_STROBE_HI	{ _NOP(); _NOP(); sbi( LCD_CTL_PORT, LCD_EN_bit); _NOP(); _NOP(); } 
#define LCD_STROBE_LO	{ _NOP(); _NOP(); cbi( LCD_CTL_PORT, LCD_EN_bit); _NOP(); _NOP(); } 
/// Single LCD strobe action:
#define LCD_STROBE	{ LCD_STROBE_HI; LCD_STROBE_LO; } 

/***********************************************************************/
/*      LCD Module Params:                                             */
/***********************************************************************/

#ifdef LCD16x4
   #define LCD_Lines   4            // Number Of Lines on LCD
   #define LCD_Chars   16           // Number Of Characters per Line on LCD
#endif   // LCD16x4

#ifdef LCD10x4
   #define LCD_Lines   4            // Number Of Lines on LCD
   #define LCD_Chars   10           // Number Of Characters per Line on LCD
#endif   // LCD10x4

#ifdef LCD20x2
   #define LCD_Lines   2            // Number Of Lines on LCD
   #define LCD_Chars   20           // Number Of Characters per Line on LCD
#endif   // LCD20x2

#ifdef LCD16x2
   #define LCD_Lines   2            // Number Of Lines on LCD
   #define LCD_Chars   16           // Number Of Characters per Line on LCD
#endif   // LCD16x2

#ifdef LCD12x2
   #define LCD_Lines   2            // Number Of Lines on LCD
   #define LCD_Chars   12           // Number Of Characters per Line on LCD
#endif   // LCD16x2

#ifdef LCD16x1
   #define LCD_Lines   1            // Number Of Lines on LCD
   #define LCD_Chars   16           // Number Of Characters per Line on LCD
#endif   // LCD16x1

#ifdef LCD16x1x2
   #define LCD_Lines   2            // Number Of Lines on LCD
   #define LCD_Chars   8            // Number Of Characters per Line on LCD
#endif   // LCD16x1x2

extern LCD_Bank unsigned char LCD_X;      // Location of cursor on LCD : X
extern LCD_Bank unsigned char LCD_Y;      // Location of cursor on LCD : Y

#ifdef UseLCDBuf
extern volatile LCD_Bank unsigned char LCD_Buf[];    // LCD DDRAM Buffer
#endif  //UseLCDBuf

#define LCD_h
#endif      // LCD_h

