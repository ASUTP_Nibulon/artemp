/** \file
	Dallas 1-Wire protocol defines
	***
	by GrayCat.
*/
#ifndef OneWire_h		///< Prevent doubling

// ==========================================================================
// ====                 1-Wire Commands:
// ==========================================================================
#define dscmd_Search				0xF0		///< Search ROM command in the auto-detect sequence
#define dscmd_ReadROM			0x33		///< Read single Dallas ROM
#define dscmd_MatchROM			0x55		///< Appeal to given ROM address
#define dscmd_SkipROM			0xCC		///< Broadcast Message
#define dscmd_SearchAlr			0xEC		///< Search Alarmed Dallases
#define dscmd_ConvertT			0x44		///< Command: Convert Temperature
#define dscmd_ReadScratch		0xBE		///< Read ScratchPad
#define dscmd_WrScratch			0x4E		///< Write ScratchPad
#define dscmd_CopyScratch		0x48		///< Copy ScratchPad
#define dscmd_ReadEE				0xB8		///< Read from EEPROM
#define dscmd_ReadPower			0xB4		///< Read Power Supply Status


// ........ Channels' Statuses: .....................
#define chst_Empty            0        ///< No sensors on Channel
#define chst_KZ2Gnd           -1       ///< Short cirquit to GND
#define chst_KZ2Pwr           -2       ///< Short cirquit to Power

#define dsst_OK					0			///< Dallas read OK!
#define dsst_CRC           	1			///< Bad CRC
#define dsst_KZ2Gnd           2       	///< Short cirquit to GND
#define dsst_KZ2Pwr           3        ///< Short cirquit to Power
#define dsst_NoAns           	4        ///< No Dallas answer
#define dsst_BadAddr          5        ///< Bad Dallas address
#define dsst_BadTemp          6        ///< Bad Temperature from Dallas 

// ==========================================================================
// ====                 T Y P E S :
// ==========================================================================

/// Dallas Retries descriptor:
typedef struct DsTries_s
		{
		unsigned TryCnt		: 7;			///< Current Try (max.127)
		unsigned TempValid	: 1;			///< Valid temperature read
		} DsTries_t;

// ==========================================================================
#define OneWire_h		///< Prevent doubling
#endif	// OneWire_h		///< Prevent doubling