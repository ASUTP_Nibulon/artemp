/** \file 
        Firmware for "ARTemp" - Autonomous Registrator of Temperature
        ***
		  Main file.
		  ***
        v0.2 by GrayCat
        \date 01-03-2007 v0.2 
	\brief Main file of Firmware for "ARTemp"
*/

/** \mainpage
	Firmware for "ARTemp" - Autonomous Registrator of Temperature
	***
	Прибор "Автономный регистратор температур" (далее - Прибор) предназначен для обеспечения автономного 
	измерения и регистрации температуры зерна в хранилищах с помощью набора термодатчиков, с последующим 
	переносом данных в персональный компьютер (ПК) по интерфейсу USB. Прибор также способен определять 
	подключенные наборы термодатчиков (термоподвески), осуществлять их привязку к физическому местоположению 
	датчиков. Предусмотрена подзарядка встроенного аккумулятора от порта USB.
	***
	Прибор построен на базе микроконтроллера Atmel ATMega128, с использованием USB-моста FT2232C для обмена
	данными с ПК, а также для программирования микроконтроллера. Программное обеспечение написано 
	на языке C с использованием RTOS JacOS. Для компиляции применяется gcc из комплекта WinAVR.
*/
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <avr/signal.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/delay.h>
#include <compat/deprecated.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

	// ................ JacOS RTOS:  ................................
#define  OS_MAIN            ///< "main()" location -- for JacOS
#include    <jacos.h>         ///< JacOS RTOS kernel
#include    <jamain.h>

	// ............. AVRlib:  ...................
#include "global.h"             	///< include our global settings           
#include "rprintf.h"    			///< include printf function library               

#include "binary_c.h"				// Binary "0B0xxx" consts
#include "1-Wire.h"             	// Local
#include "lcd.h"
#include "ARTemp.h"					// Application's
#include "Messages.h"				// Application's Messages (#defines)
#include "UI.h"
#include "ModBus.h"          		// ModBUS headers interface 

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                        V A R I A B L E S :                       ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ........... Power: .......................
unsigned short		PowerOffTO;						///< PowerOff TimeOut
BattState_t				BattState;						///< Battery state
unsigned char			USBpower;						///< State of Power Source

	// ........... Display:  ....................
unsigned char			ShowChan;						///< Channel to show
unsigned char			ShowChanMask;					///< Mask for Channel to show
unsigned char			ShowSensor;
signed char			StartCursor, EndCursor;		///< Start and end of blinking cursor

	// ................ I2C EEPROM: .............
uint16_t					eep_CurRecIdx;					///< Index of Current Operating Record
eep_Record_t			eep_Buffer;						///< Buffer for single Record		
uint16_t					eep_PrevRecIdx;				///< Previous Index of Current Operating Record

	// ................. I2C Real-Time Clock: ..............
rtc_Data_t				rtc_Data;						///< Real-Time Data;

	// ......... Dallases: ....................
uint8_t 					ds_Addrs[NumChannels][kol_dat][8];	///< Operating Dallas addresses
unsigned char			ds_Status[NumChannels][kol_dat];		///< Dallas Statuses
unsigned char			ds_NumOnChan[NumChannels];				///< Number of sensors on channel
unsigned char			TotalSensors;

// JJJJJJJJJJJJJJJJJJJJJJJJJJJ   JacOS related:      JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ
   // ......... Task vars:   ...................
OST_TASK_T  task_UI;										///< Task var. -- LCD writes
OST_TASK_T  task_Dallas;								///< Task var. -- Dallas communication
OST_TASK_T  task_UART;									///< Task var. -- UART communication
OST_TASK_T  task_Menu;									///< Task var. -- Menu functionality
OST_TASK_T  task_Intervals;							///< Task var. -- Long Intervals
   // ........ Flag vars:  ..................
OST_FLAGS   TaskFlags;									///< Individual tasks flags, see #gTaskFlags constants

   // ++++++++++++ Declarations: +++++++++
__task void T_UI( void );
__task void T_Dallas( void );
__task void T_UART( void );
__task void T_Menu( void );
__task void T_Intervals( void );
	// ............. Message Queues:  .........
OST_MSGQUEUE			lcd_msgq;						///< JacOS Message Queue variable
OST_MSGQ_DESCR			lcd_mqdscr;						///< JacOS Message Queue Descriptor
OST_MSG_P				lcd_mbuff[lcd_QueueLen];	///< JacOS Message Queue Buffer

OST_MESSAGE				UI_event;						///< Message about User Interface Event
OST_MESSAGE				Dallas_event;					///< Message to Dallas process
OST_SEMAPHORE			Dallas_Sem;						///< Semaphore for Dallas Operations
OST_SEMAPHORE			I2C_Sem;							///< Semaphore for I2C Operations


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++             	M O D B U S   v a r i a b l e s :                   ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define d_MBvarsQuant			13u					///< Define for Quantity of vars in MB_Vars[]
const uchar MBvarsQuant = d_MBvarsQuant; 			///< Constant for Quantity of vars in MB_Vars[]

/// List of program variables accessible by ModBUS
s_MB_Vars MB_Vars[d_MBvarsQuant]=							
               {
                  {0x000B,	&BattState},			///< Battery State
                  {0x000C,	&PowerOffTO},			///< Power-Off TimeOut
                  {0x000F,	&eep_CurRecIdx},		///< Index of Current Operating Record
						{0x0010,	&eep_Buffer.Buffer[0x00] },	///< Least significant word of eep_Buffer
						{0x0011,	&eep_Buffer.Buffer[0x02] },	///< Word of eep_Buffer
						{0x0012,	&eep_Buffer.Buffer[0x04] },	///< Word of eep_Buffer
						{0x0013,	&eep_Buffer.Buffer[0x06] },	///< Word of eep_Buffer
						{0x0014,	&eep_Buffer.Buffer[0x08] },	///< Word of eep_Buffer
						{0x0015,	&eep_Buffer.Buffer[0x0A] },	///< Word of eep_Buffer
						{0x0016,	&eep_Buffer.Buffer[0x0C] },	///< Word of eep_Buffer
						{0x0017,	&eep_Buffer.Buffer[0x0E] },	///< Most significant word of eep_Buffer
						{0x0018,	&rtc_Data.Seconds},			///< Real Time Seconds
						{0x0019,	&rtc_Data.Minutes},			///< Real Time Minutes
               };   // End of MB_Vars[]

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                       P R O C E D U R E S :                      ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Interrupt: Timer0:
SIGNAL(SIG_OVERFLOW0)
	{
	TCNT0 += (256-230);             // Update Timer0 to give 230 ticks/interrupt
	OS_Timer();       // ++++++++++++++++++   OK, OS Timer tick !!! +++++++++++
		
		// ............... LEDs work: .......................
	LEDs_port = 0xFF;					// Put down all LEDs
	PORTE |= _BV(led_line2);				
	PORTE ^= _BV(led_lineA);
	PORTE ^= _BV(led_lineB);
	
	if ( PORTE & _BV(led_lineB) )
		{		// +++++++++++++++++ RED LEDs +++++++++++++++
		LEDs_port &= ~ShowChanMask;
		
			// ............. Display Battery state:  .................
		if ( 		(bst_Charging==BattState) 
				|| ( (bst_Discharged==BattState) /*&& (led_bool & 1) */ )
			)
			{		// +++++++++++++++++ Need4Batt. display Red ++++++++++++++++++++++
			PORTE &= ~_BV(led_line2);		// Turn On  LED
			};		// -------------- Need4Batt. display Red -------------------------
		}		// -------------- RED LEDs -----------------
		else
			{		// ++++++++++++++++++ Green LEDs  +++++++++++++++++++++++
			LEDs_port = ShowChanMask;			
				// ............. Display Battery state:  .................
			if ( 		(bst_Charging==BattState) 
					|| (bst_Normal==BattState) 
					|| (bst_Charged==BattState) 
				)
				{		// +++++++++++++++++ Need4Batt. display Red ++++++++++++++++++++++
				PORTE &= ~_BV(led_line2);		// Turn On  LED
				};		// -------------- Need4Batt. display Red -------------------------
			};		// ------------------- Green LEDs -----------------------
	
	// ================= Some job for ModBus ASCII: =======================
	if (MB_Delay)  // ModBus Delay active?...
		{     // ++++++++++++++ ModBus Delay ++++++++++++++++

		if ( !(--MB_Delay))
			{     // ++++++++++++++ ModBus Delay Expired! ++++++++++++++++
			switch (MB_Stage)
				{
				case MBStage_PreGap:       // PreGap finished,
					MB_Stage = MBStage_Tx;           // TX stage
					UDR1 = ':';                     // Frame start
					UCSR1B |= _BV(TXCIE1);		// Enable TX int.
					UCSR1B &= ~_BV(RXCIE1);		// Disable RX int.
					break;
				case MBStage_WaitEnd:      // Now, End of Packet!
					if ( !(UCSR1A & _BV(TXC1) ) )
						{     // +++++++++++ Buffer not clean, wait again .......
						MB_Delay = 1;     // Minimum delay, see actual "TX End" by flag
						break;
						};    // --------- Buffer not clean, wait again ----------
					MB_Stage =  MBStage_Idle;
					MB_Wait4Reply = 0;							
					UCSR1B &= ~_BV(TXCIE1);		// Disable TX int.
					MB_RxByte=RCREG;        // Clear Receiver
					UCSR1B |= _BV(RXCIE1);		// Enable RX int.
					break;      // ---------- End of "case MBStage_WaitEnd" ---------

				case MBStage_Rx:        // ++++ Timeout while Receive! +++++++++
//               case MBStage_WaitEnd:
					ResetRX();                    // Reset Receiver
					MB_Error = MBer_FrameIncomp;     // Indicate error
					break;      // ---------- End of "Timeout while Receive!" ---------
				};    // ----------- End of "switch (MB_Stage)" --------
			};    // ----------- End of "ModBus Delay Expired!" --------

		};    // --------- End of "ModBus Delay" ------------------

	};		// ------------------ SIGNAL(SIG_OVERFLOW0) ------------------------
// --------------------------------------------------------------------------

/// Interrupt: Timer1:
SIGNAL(SIG_OVERFLOW1)
	{
	
	}
// --------------------------------------------------------------------------
	
/// Put a character into OS message queue:
void PutCh_queue(unsigned char Ch)
	{
	OS_Post_MsgQ( &lcd_msgq, (OST_MSG_P) (int)Ch);
	};		// ------------------- PutCh_queue() ------------------------

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// OS Task: LCD writes, Keyboard read
__task void T_UI( void )
   {
	static OST_MSG_P	xx;
	static uint8_t		Ch;

	for (;;)
		{		// +++++++++++++++ Main Task loop ++++++++++++++++++++++++
		OS_Delay(1 MSEC);         // 1msec inter-symbol delay for State
		if (KbCnt<0xFF)		// saturate
			KbCnt++;				// update counter
	
		OS_Wait_MsgQT(&lcd_msgq, xx, 1);		// Wait for symbol or 1 tick
		if (! OS_Timeout() )
			{		// +++++++++++++++++ Really got symbol ++++++++++++++++
			Ch = (char)( (int)xx) ;			// Get the symbol
			putch( Ch );
			if ( '\f'==Ch)		// "Clear screen"?...
				OS_Delay(2 MSEC);	// ...additional wait
			};		// ----------------- Really got symbol -----------------
			// .............. Now, look at Keyboard:  .....................
		Ch = KeyPressed();		// Analyse Keyboard,
		if ( Ch )
			{		// ++++++++++++++++ Some Key pressed: +++++++++++++++++
			OS_Post_Msg(&UI_event, &Ch);
			PowerOffTO = Power_TimeOut;		// Reinit Power TimeOut
			};		// ----------------- Some Key pressed ------------------
		};		// ----------------- Main Task loop ----------------------
	};		// ---------------------- T_UI() ----------------------------------
// --------------------------------------------------------------------------

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Dallases poll:
__task void T_Dallas( void )
	{
	static int					*Ds_msg;		// Message to Dallas
	static unsigned char	stat;
	
	for (;;)
		{		// +++++++++++++++++ Main Task loop +++++++++++++++++++++++++
		OS_Delay(1);			// Don't hurry! ;-) 
		OS_Wait_Msg(&Dallas_event, Ds_msg);
		switch (*Ds_msg)
			{		// ++++++++++++++ According to command,... ++++++++++
			case dsmsg_Reset:					// Send Reset to all Dallases
					{
					
					ds_OutPort = 0xff;			// Pull down,
					OS_Delay(1 MSEC /2 );				// Wait 0.5msec
					ds_OutPort = 0;				// Release
					_delay_us(50);             // Real 30u until Presence Pulse
					stat = ds_InPort;                // Sample pins,
					OS_Delay(1 MSEC /2);             // After-Presence delay: 
					};              // -------------- res_dal() --------------------
					break;		// ------ dsmsg_Reset:
			case dsmsg_SearchROMs:				///< Search ROMs present
			case dsmsg_ReadTemps:				///< Convert & read Temperatures
					break;
			};		// ------------- switch (*Ds_msg) ----------------------
		};		// ----------------- Main Task loop  ------------------------
	};		// -------------------- T_Dallas() ----------------------------
// --------------------------------------------------------------------------
 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// UART Communications:
__task void T_UART( void )
	{
	static uint8_t		RecvResult;
	for (;;)
		{		// +++++++++++++++++ Main Task loop +++++++++++++++++++++++++
		// OS_Delay(10);		
		OS_Wait_Flag(&TaskFlags, tf_RxPacket);			// Wait for received packet
		OS_Clear_Flag(&TaskFlags, tf_RxPacket);		// Reset flag
		RecvResult = MODBUS_Receive();					// Get Receive result
		
		switch ( RecvResult )
			{
			case MBrx_SetCommand:     ///< "Set"-type command received
					if (MB_Buf[0])           // See if it was NOT a Broadcast
						{
							// Return ACK
						MB_Replying = 1;
						MODBUS_Send(MB_SlaveAddr,MB_Buf[1] , 4 , NULL);
						}
					MB_Error=MBer_None;
					break;		// ------------- MBrx_SetCommand ---------------
			case MBrx_GetCommand:     ///< "Get"-type command received
					if (MB_Buf[0])           // See if it was NOT a Broadcast
						{
// debug:

// prepare address
if ( /* eep_PrevRecIdx != eep_CurRecIdx */ 4==MB_Buf[1])
{
static u08 packet[4];
#define i2cAddr 0xA0
static OST_ERR_MSG	Sem_ERR_MSG;
#define EEP_8kbit

OS_Wait_SemT(&I2C_Sem, 10 MSEC);		// Wait for I2C Ready

#ifdef EEP_256kbit
packet[0] = (eep_CurRecIdx>>4);
packet[1] = ( (eep_CurRecIdx<<4)&0x00F0);
// send memory address we wish to access to the memory chip
i2cMasterSend(i2cAddr, 2, packet);
#endif // EEP_256kbit

#ifdef EEP_8kbit
packet[0] = ( (eep_CurRecIdx<<4)&0x00F0);
// send memory address we wish to access to the memory chip
i2cMasterSend(i2cAddr, 1, packet);
#endif // EEP_8kbit

OS_Wait_SemT(&I2C_Sem, 1 MSEC);		// Wait for I2C Ready after Send

// retrieve the data at this memory address
if (! OS_Timeout() )
	{		// +++++++++++++ I2C Send OK! +++++++++++++
	i2cMasterReceive(i2cAddr, 16);
	OS_Wait_SemT(&I2C_Sem, 10 MSEC);		// Wait for I2C Ready after Receive
	if ( OS_Timeout() )		// Error?...
		eep_Buffer.Buffer[8]++;		// Dummy op
		else		// I2C Receive OK! 
			memcpy( eep_Buffer.Buffer, I2cReceiveData, 16);		// return data
	};		// ------------ I2C Send OK! ---------------------
OS_Post_BSemR(&I2C_Sem, Sem_ERR_MSG);		// Release I2C - Raise Semaphore!
};
						  // Return ACK
						MB_Replying = 1;
						MODBUS_Send(MB_Buf[0],MB_Buf[1] , MB_BufBytes-4 , NULL);
						};
					MB_Error=MBer_None;
					break;		// -------------- MBrx_GetCommand ---------------
			case MBrx_LongGetCmd:
					if (MBF_RdDataRec==MB_Buf[1])
						{		// +++++++++++++++ READ DATA RECORD command +++++++++++++

						memcpy( MB_Buf+2, &eep_Buffer, 16);
						
						MODBUS_Send(MB_Buf[0], MB_Buf[1], 16, NULL);
						};		// ------------- READ DATA RECORD command ---------------
			default:
					break;
			};    // ------------- switch (MODBUS_Receive()) -----------
		eep_PrevRecIdx = eep_CurRecIdx;
		};		// ----------------- Main Task loop  ------------------------
	};		// -------------------- T_Dallas() ----------------------------
// --------------------------------------------------------------------------

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
	Menu works: UserMode switching.
*/
__task void T_Menu( void )
	{
	static int			*UI_msg;		// Message from UI
	for (;;)
		{		// +++++++++++++++++ Main Task loop +++++++++++++++++++++++++
		OS_Wait_MsgT(&UI_event, UI_msg, 10 MSEC);
		if (! OS_Timeout() )
			{		// +++++++++++++++++ Really got UI event ++++++++++++++++
			
			switch ( (char)*UI_msg )
				{
				case kb_Left:
						ShowChan = (ShowChan + (NumChannels-1) ) % NumChannels;
						break;
				case kb_Right:
						ShowChan = (++ShowChan) % NumChannels;
						break;
				};		// -------------- switch ( (char)*UI_msg ) ---------------
// debug:
ShowChanMask = 1<<ShowChan;				// Update mask
rprintf(msg_Query, BattState, ShowChan, ShowSensor, ds_Status[ShowChan][ShowSensor] );
			};		// ----------------- Really got UI event ----------------
			
		};		// ----------------- Main Task loop  ------------------------
	};		// -------------------- T_Menu() ----------------------------
// --------------------------------------------------------------------------

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
	Task: Long intervals: Dallases poll, Cursor blink, Auto-Shutdown timer, 
	Battery analyses.
*/
__task void T_Intervals( void )
	{
	static u08 packet[2];
	static OST_ERR_MSG	Sem_ERR_MSG;
#define i2c_RTC_Addr 0xD0
	
	for (;;)
		{		// +++++++++++++++++ Main Task loop +++++++++++++++++++++++++
		WDR();		// Reset watchdog
		OS_Delay( 500 MSEC );		
			
			// ............. Determine Battery state:  ..................
		if (USBpower)
			{			// +++++++++++++++ USB powered ++++++++++++++++++
			ChrgSenseRelease;		// Release strong PullUp
			NOP();
			if (ChargingState)		// =="1" with weak pull-up?...
				BattState = bst_Normal;
				else
					{		// ++++++++++++++++ Some pull-down present +++++++++++++++++
					ChrgSensePullUp;
					NOP();
					if (ChargingState)		// =="1" with strong pull-up?...
						BattState = bst_Charged;
						else		// Strong pull-down?...
							BattState = bst_Charging;
					};		// --------------- Some pull-down present ---------------
			ChrgSenseRelease;		// Finally, Release strong PullUp
			if ( 2 >= (--PowerOffTO) )
				PowerOffTO = Power_TimeOut;
			}			// ----------------- USB powered -----------------------
			else
				{		// +++++++++++++++ Battery Powered +++++++++++++++++
				if ( !(ACSR & _BV(ACO) ) )
					{		// ++++++++++++++ Normal battery ++++++++++++++
					BattState = bst_Normal;
					PORTG |= _BV(3);		// debug out
					}
					else
						{		// ++++++++++++++ Bad Battery: +++++++++++++++
						BattState = bst_Discharged;
						PORTG &= ~_BV(3);		// debug out
						PowerOffTO = 4;	// Force power-off
						}		// ------------- Bad Battery: ------------------
						// Work with Power TimeOut
				if (PowerOffTO)
					PowerOffTO--;
					else
						{
						PowerSw_OFF;
						TIMSK = 0; 	// Disable Timer0 and Timer1 int.
						cli();
						LEDs_port = 0x00;					// Lit Up all LEDs
						PORTE &= ~_BV(led_line2);				
						PORTE &= ~_BV(led_lineA);
						PORTE &= ~_BV(led_lineB);
						set_sleep_mode(SLEEP_MODE_PWR_DOWN);
						WDR();
						sleep_mode();
						sleep_enable();
						sleep_cpu();
						SLEEP();
						for(;;)
							{
							PowerSw_OFF;
							WDR();
							};
						};
				};		// --------------- Battery Powered -----------------
			// .......... Determine power source:  ..............
		PowerPinIn;		// Turn into Input,
		_delay_us(1);	// 1usec
		USBpower = (PowerPin) ? 0 : 1;
		PowerPinOut;		// Return to Out
			// ................ Read RTC: ................
		OS_Wait_SemT(&I2C_Sem, 10 MSEC);		// Wait for I2C Ready
		if (! OS_Timeout() )
			{		// +++++++++++++ I2C Ready! +++++++++++++
			packet[0] = 0;		// Register number: Seconds
			i2cMasterSend( i2c_RTC_Addr, 1, packet);		// send memory address we wish to access to the RTC chip
			OS_Wait_SemT(&I2C_Sem, 2 MSEC);		// Wait for I2C Ready
			if (! OS_Timeout()  )		// Send OK?...
				{
				i2cMasterReceive( i2c_RTC_Addr, 7);		// retrieve the data at this memory address
				OS_Wait_SemT(&I2C_Sem, 2 MSEC);		// Wait for I2C Receive
				if (! OS_Timeout()  )	// retrieve the data at this memory address
					{		// ++++++++++ Receive OK +++++++++
					memcpy( &rtc_Data, I2cReceiveData, 7);
					if (rtc_Data.Seconds & 0x80)		// Clock on-hold?...
						{		// +++++++++++++ Clear "Clock Hold" ++++++++++++++
						packet[0] = 0;		// Register number: Seconds
						packet[1] = rtc_Data.Seconds & (u08)~0x80;		// Register value: Init
						// send memory address we wish to access to the memory chip
						i2cMasterSend( i2c_RTC_Addr, 2, packet);
						OS_Wait_SemT(&I2C_Sem, 1 MSEC);		// Wait for I2C Ready
						};		// -------------- Clear "Clock Hold" ------------
						
// debug:
rprintf(msg_Time, rtc_Data.Minutes, rtc_Data.Seconds );
					};		// ------------ Receive OK ---------------
				};
			};	// ----------- I2C Ready! -----------
		OS_Post_BSemR(&I2C_Sem, Sem_ERR_MSG);		// Release I2C - Raise Semaphore!

		};		// ----------------- Main Task loop  ------------------------
	};		// -------------------- T_Intervals() ----------------------------
// --------------------------------------------------------------------------

// =================================================================
/// .................. MAIN Function:  .................
int main(void)
	{
	MainInit();			// Hardware initializations
	
	      // ******************** Tasks Init :
   OS_Init();
	OS_Task_Create(T_Dallas, &task_Dallas, 2); // High prio
	OS_Task_Create(T_UART, &task_UART, 2); // High prio
   OS_Task_Create(T_UI, &task_UI, 1); // Med prio
	OS_Task_Create(T_Menu, &task_Menu, 1); // Med prio
	OS_Task_Create(T_Intervals, &task_Intervals, 1); // Low prio
	
	OS_MsgQ_Create(&lcd_msgq, &lcd_mqdscr, lcd_QueueLen, lcd_mbuff);		// LCD message queue
	OS_Msg_Create(&UI_event, 0);
	OS_Msg_Create(&Dallas_event, 0);
	OS_Sem_Create(&Dallas_Sem, 1);		// Init Semaphore
	OS_Sem_Create(&I2C_Sem, 1);		// Init Semaphore
	OS_Flag_Create(&TaskFlags, 0);		// create Flags
	
	sei();							// Enable Interrupts!
	
	rprintfInit( /*putch*/ PutCh_queue );
	rprintf( msg_Hello1, __TIME__ );
	WDR();							// Reset watchdog
	
// ========================== Start of MAIN Loop ==========================
   for (;;)
      {  // ++++++++++++++++++++ MAIN Loop +++++++++++++++++++++++++++++++++
      OS_Scheduler();   // OS tasks,...      
      };    // ----------------- End of MAIN Loop --------------------------
	
	return(0);		// dummy
	};              // ------------------------ MAIN () ------------------------------------
// --------------------------------------------------------------------------

/// UART Receive interrupt
SIGNAL(SIG_UART1_RECV)
	{
	if ( UCSR1A & (_BV(FE1) | _BV(DOR1)  ) )
		{			// +++++++++++++ Byte error ++++++++++++
		MB_RxByte=UDR1;        // Receive byte
		return;
		};			// -------------- Byte error -------------
	MB_RxByte=UDR1;        // Receive byte
	RxChar(MB_RxByte);
	if (MB_RxPending)
		OS_Set_FlagI(&TaskFlags, tf_RxPacket);		// Packet Ready!
	};		// ----------------- SIG_UART1_RECV -------------------------
	
/// UART Transmit interrupt
SIGNAL(SIG_UART1_TRANS)
	{
		// ** Check for finish ***
	if ( (MB_BufIdx < MB_BufBytes) && (MB_Stage==MBStage_Tx) )
		UDR1 = TxChar();    //  OK, send next byte
		else
			{      // ++++++++++  Post-Process +++++++++++
			switch (MB_Stage)
				{
				case MBStage_Tx:     // Just finished TX from Buffer
					MB_Stage=MBStage_PostGap;
					UDR1 = 0x0D;     // Send "CR"
					break;   // -------- End of "case MBStage_Tx" ----------
	
				case MBStage_PostGap:   // Finished "CR" transmit, send "LF"
					UDR1 = 0x0A;     // Send "LF"
					MB_Stage=MBStage_WaitEnd;
					MB_Delay = 1;     // Minimum delay, see actual "TX End" by flag
					break;   // -------- End of "case MBStage_Tx" ----------
			
				case MBStage_WaitEnd:      // Now, End of Packet!
					MB_Stage =  MBStage_Idle;
					MB_Wait4Reply = 0;							
					UCSR1B &= ~_BV(TXCIE1);		// Disable TX int.
					MB_RxByte=RCREG;        // Clear Receiver
					UCSR1B |= _BV(RXCIE1);		// Enable RX int.
					break;      // ---------- End of "case MBStage_WaitEnd" ---------
				};       // --------- End of "switch (MB_Stage)" -----------
			};    // ----------- End of "Post-Process " ------------
	};		// ----------------- SIG_UART0_TRANS ------------------------
	

/// I2C (TWI) interrupt service routine
SIGNAL(SIG_2WIRE_SERIAL)
	{
	// read status bits
	u08 status = inb(TWSR) & TWSR_STATUS_MASK;

	switch(status)
		{
		// Master General
		case TW_START:						// 0x08: Sent start condition
		case TW_REP_START:					// 0x10: Sent repeated start condition
			i2cSendByte(I2cDeviceAddrRW);		// send device address
			break;
		
		// Master Transmitter & Receiver status codes
		case TW_MT_SLA_ACK:					// 0x18: Slave address acknowledged
		case TW_MT_DATA_ACK:				// 0x28: Data acknowledged
			if(I2cSendDataIndex < I2cSendDataLength)
				i2cSendByte( I2cSendData[I2cSendDataIndex++] );		// send data			
				else
					{
					i2cSendStop();		// transmit stop condition, enable SLA ACK
					I2cState = I2C_IDLE;		// set state
					OS_Post_BSemI(&I2C_Sem);		// Raise Semaphore!
					}
			break;
		case TW_MR_DATA_NACK:				// 0x58: Data received, NACK reply issued			
			I2cReceiveData[I2cReceiveDataIndex++] = inb(TWDR);		// store final received data byte
			OS_Post_BSemI(&I2C_Sem);		// Raise Semaphore!
			// continue to transmit STOP condition
		case TW_MR_SLA_NACK:				// 0x48: Slave address not acknowledged
		case TW_MT_SLA_NACK:				// 0x20: Slave address not acknowledged
		case TW_MT_DATA_NACK:				// 0x30: Data not acknowledged
			i2cSendStop();		// transmit stop condition, enable SLA ACK
			I2cState = I2C_IDLE;		// set state
			break;
		case TW_MT_ARB_LOST:				// 0x38: Bus arbitration lost
		//case TW_MR_ARB_LOST:				// 0x38: Bus arbitration lost			
			outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT));	// release bus			
			I2cState = I2C_IDLE;		// set state
			//outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT)|BV(TWSTA));	// release bus and transmit start when bus is free
			break;
		case TW_MR_DATA_ACK:				// 0x50: Data acknowledged			
			I2cReceiveData[I2cReceiveDataIndex++] = inb(TWDR);		// store received data byte
			// fall-through to see if more bytes will be received
		case TW_MR_SLA_ACK:					// 0x40: Slave address acknowledged
			if(I2cReceiveDataIndex < (I2cReceiveDataLength-1))				
				i2cReceiveByte(TRUE);		// data byte will be received, reply with ACK (more bytes in transfer)
				else					
					i2cReceiveByte(FALSE);		// data byte will be received, reply with NACK (final byte in transfer)
			break;
	
		// Misc
		case TW_NO_INFO:					// 0xF8: No relevant state information
			// do nothing
			break;
		// case TW_BUS_ERROR:					// 0x00: Bus error due to illegal start or stop condition			
		default:
			outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT)|BV(TWSTO)|BV(TWEA));		// reset internal hardware and release bus
			I2cState = I2C_IDLE;		// set state
			break;
		};		// -------------- switch(status) ----------------------------
	};		// -------------------- SIGNAL(SIG_2WIRE_SERIAL) ------------------------
