/** \file
	Dallas 1-Wire protocol defines. 
	***
	by GrayCat.
	\brief Dallas 1-Wire protocol defines. 
*/
#ifndef OneWire_h		///< Prevent doubling

// ==========================================================================
// ====                 1-Wire Commands:
// ==========================================================================
#define dscmd_Search				0xF0		///< Search ROM command in the auto-detect sequence
#define dscmd_ReadROM			0x33		///< Read single Dallas ROM
#define dscmd_MatchROM			0x55		///< Appeal to given ROM address
#define dscmd_SkipROM			0xCC		///< Broadcast Message
#define dscmd_SearchAlr			0xEC		///< Search Alarmed Dallases
#define dscmd_ConvertT			0x44		///< Command: Convert Temperature
#define dscmd_ReadScratch		0xBE		///< Read ScratchPad
#define dscmd_WrScratch			0x4E		///< Write ScratchPad
#define dscmd_CopyScratch		0x48		///< Copy ScratchPad
#define dscmd_ReadEE				0xB8		///< Read from EEPROM
#define dscmd_ReadPower			0xB4		///< Read Power Supply Status

// ==========================================================================
#define OneWire_h		///< Prevent doubling
#endif	// OneWire_h		///< Prevent doubling