/** \file
        Firmware for "ARTemp" - Autonomous Registrator of Temperature
        ***
		  Utilities file.
		  ***
        v0.2 by GrayCat
        v0.2 :  01-03-2007
	\brief Utilities file.
*/

#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <avr/signal.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/delay.h>
#include <compat/deprecated.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

	// ................ JacOS RTOS:  ................................
#include    <jacos.h>         // JacOS RTOS kernel
#include    <jamain.h>

	// ............. AVRlib:  ...................
#include "global.h"             // include our global settings           
#include "rprintf.h"    // include printf function library               

#include "binary_c.h"				// Binary "0B0xxx" consts
#include "1-Wire.h"             	// Local
#include "lcd.h"
#include "ARTemp.h"					// Application's
#include "ModBus.h"          /* ModBUS headers interface  */


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                        V A R I A B L E S :                       ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// \ingroup I2C 
/// @{
// I2C state and address variables
volatile eI2cStateType I2cState;
u08 I2cDeviceAddrRW;
// send/transmit buffer (outgoing data)
u08 I2cSendData[I2C_SEND_DATA_BUFFER_SIZE];
u08 I2cSendDataIndex;
u08 I2cSendDataLength;
// receive buffer (incoming data)
u08 I2cReceiveData[I2C_RECEIVE_DATA_BUFFER_SIZE];
u08 I2cReceiveDataIndex;
u08 I2cReceiveDataLength;
/// @}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                       P R O C E D U R E S :                      ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
	Main initialization tasks:
	- Init I/O pins;
	- Set up Timers;
	- Set up UART;
	- Set up Analog Comparator;
	- Init LCD.
 */
void MainInit(void)
	{
	DDRA = 0;					// Dallas Inputs
	DDRB = 0xFD;					// Power; Buttons
	DDRC = 0xff;				// Dallas Outputs
	DDRD = 0xFF & ~0x04 & ~0x03; // LCD Data; I2C; UART
	DDRE = 0x04 | 0x30 ; 			// Programming; LEDs, Sensors
	DDRF = 0xFF;				// LEDs array
	DDRG = 0xFF;				// LCD controls
	
	WDR();							// Reset watchdog
	WDTCR = _0b00011111;			// Enable WatchDog! with Maximum Period
	WDTCR = _0b00011111;			// Enable WatchDog! with Maximum Period

	
	if ( MCUCSR & _BV(BORF) )
		{		// +++++++++++++++++ Was restarted after BOD... +++++++++++++++
		PowerSw_OFF;
		cli();								// No ints.
		LEDs_port = 0x00;					// Lit Up all LEDs
		PORTE &= ~_BV(led_line2);				
		PORTE &= ~_BV(led_lineA);
		PORTE &= ~_BV(led_lineB);
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		sleep_mode();
		sleep_enable();
		sleep_cpu();
		SLEEP();
		for(;;)
			PowerSw_OFF;
		};		// ----------------- Was restarted after BOD... ---------------
	
	PowerPinOut;
	PowerSw_ON;					// Power On immediatelly
	PowerOffTO = Power_TimeOut;			// Init Time-out timer
			
	// TCCR0 = 5; //T0, Ck/128 14745600 Hz /128/256 =  450 Hz
	// T0: Ck/32 14745600 Hz /32 /230 =  2003 Hz
	TCCR0 = _0b00000011; // Timer0 working mode:
		//      ||||||||
		//      |||||+++---->   CS02:CS00       : Clock Select: "000"="Stopped", "011"="T0src/32", "100"="T0src/64", "101"="T0src/128"
		//      ||++-------->   COM01:COM00     : Compare Match Output Mode: "00"="Normal port operation, OC0 disconnected"
		//      |+--+------->   WGM01:WGM00     : Waveform Generation Mode: "00"=Normal
		//      +----------->   FOC0                    : Force Output Compare

	TCCR1A = 0; //Normal mode Timer1 without compare and PWM
	TCCR1B = 4; //T1, 14.7456 Mhz Ck/256 =  57600Hz, overflow = 46080 / 57600  = 0.8 sec
	TIMSK = 5; //Enable Timer0 and Timer1
	// ................ Comparator:  .................
	SFIOR &= ~_BV(ACME);		// Multiplexer off,
	ACSR = _0b01000000;
	//        ||||||||
	//        |||||||+-->	ACIS0		: \_ Analog Comparator Interrupt Mode Select
	//        ||||||+--->	ACIS1		: / 
	//        |||||+---->	ACIC		: Analog Comparator Input Capture Enable
	//        ||||+----->	ACIE		: Interrupt Enable
	//        |||+------>	ACI		: Interrupt flag
	//        ||+------->	ACO		: Comparator Output
	//        |+-------->	ACBG		: Compare with BandGap, i.e. Internal Vref
	//        +--------->	ACD		: Comparator Disable
	
	PORTA = 0xFF;           // PullUps on Inputs
	PORTG = 0;
	PORTD = 0;
	PORTF = 0;
	sbi(PORTE,led_line2);
	PORTE |= _BV(led_lineA);
	PORTE &= ~_BV(led_lineB);
	
	lcd_init();
	MODBUS_Init(); 
	
		// ................. Init TWI / I2C for Maximum speed: .......................
	TWBR = 10;			///< TWI Clock Generator Divider, min.10
	TWSR = 0;			///< Prescaler select: '0'=1, '1'=4, '2'=16, '3'=64
		// => Baud rate = F_CPU / (16 + 2*(TWBR)*Prescaler )
		// here  Baud rate = 14745600 / (16 + 2*(10)*1 ) = 409600 baud
	TWCR = _0b00000101;		///< Control register
		//     ||||||x|
		//     |||||| +--->	TWIE: TWI Interrupt Enable
		//     |||||+----->	TWEN: TWI Enable Bit
		//     ||||+------>	TWWC: TWI Write Collision Flag
		//     |||+------->	TWSTO: TWI STOP Condition Bit
		//     ||+-------->	TWSTA: TWI START Condition Bit
		//     |+--------->	TWEA: TWI Enable Acknowledge Bit, "1"=Send ACK
		//     +---------->	TWINT: TWI Interrupt Flag, writing "1" starts TWI operations
	};		// ----------------- MainInit() ---------------------------
	
/// \ingroup I2C 
/// @{
inline void i2cSendStart(void)
	{
	// send start condition
	outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT)|BV(TWSTA));
	}

inline void i2cSendStop(void)
	{
	// transmit stop condition
	// leave with TWEA on for slave receiving
	outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT)|BV(TWEA)|BV(TWSTO));
	}

inline void i2cWaitForComplete(void)
	{
	// wait for i2c interface to complete operation
	while( !(inb(TWCR) & BV(TWINT)) );
	}

inline void i2cSendByte(u08 data)
	{
	// save data to the TWDR
	outb(TWDR, data);
	// begin send
	outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT));
	}

/**
	\param ackFlag = TRUE if recevied data should be ACK'ed; = FALSE if recevied data should be NACK'ed
*/
inline void i2cReceiveByte(u08 ackFlag)
	{
	// begin receive over i2c
	if( ackFlag )
		{
		// ackFlag = TRUE: ACK the recevied data
		outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT)|BV(TWEA));
		}
		else
			{
			// ackFlag = FALSE: NACK the recevied data
			outb(TWCR, (inb(TWCR)&TWCR_CMD_MASK)|BV(TWINT));
			}
	}

inline u08 i2cGetReceivedByte(void)
	{
	// retieve received data byte from i2c TWDR
	return( inb(TWDR) );
	}

inline u08 i2cGetStatus(void)
	{
	// retieve current i2c status from i2c TWSR
	return( inb(TWSR) );
	}

void i2cMasterSend(u08 deviceAddr, u08 length, u08* data)
	{
	u08 i;
	// wait for interface to be ready
	// while(I2cState);
	// set state
	I2cState = I2C_MASTER_TX;
	// save data
	I2cDeviceAddrRW = (deviceAddr & 0xFE);	// RW cleared: write operation
	for(i=0; i<length; i++)
		I2cSendData[i] = *data++;
	I2cSendDataIndex = 0;
	I2cSendDataLength = length;
	// send start condition
	i2cSendStart();
	};		// ------------- i2cMasterSend() -----------------------

void i2cMasterReceive(u08 deviceAddr, u08 length)
	{
	// wait for interface to be ready
	// while(I2cState);
	// set state
	I2cState = I2C_MASTER_RX;
	// save data
	I2cDeviceAddrRW = (deviceAddr|0x01);	// RW set: read operation
	I2cReceiveDataIndex = 0;
	I2cReceiveDataLength = length;
	// send start condition
	i2cSendStart();
	// wait for data
	// None in OS model!!! ::    while(I2cState);	
	};		// ------------- i2cMasterReceive() ----------------------------------

u08 i2cMasterSendNI(u08 deviceAddr, u08 length, u08* data)
	{
	u08 retval = I2C_OK;
		
	cbi(TWCR, TWIE);		// disable TWI interrupt
	
	i2cSendStart();		// send start condition
	i2cWaitForComplete();
	
	i2cSendByte( deviceAddr & 0xFE );	// send device address with write
	i2cWaitForComplete();
	
	// check if device is present and live
	if( inb(TWSR) == TW_MT_SLA_ACK)
		{
		// send data
		while(length)
			{
			i2cSendByte( *data++ );
			i2cWaitForComplete();
			length--;
			}
		}
		else
			{
			// device did not ACK it's address,
			// data will not be transferred
			// return error
			retval = I2C_ERROR_NODEV;
			}
	
	// transmit stop condition
	// leave with TWEA on for slave receiving
	i2cSendStop();
	while( !(inb(TWCR) & BV(TWSTO)) );
	
	// enable TWI interrupt
	sbi(TWCR, TWIE);
	
	return retval;
	}

u08 i2cMasterReceiveNI(u08 deviceAddr, u08 length, u08 *data)
	{
	u08 retval = I2C_OK;
	
	// disable TWI interrupt
	cbi(TWCR, TWIE);
	
	// send start condition
	i2cSendStart();
	i2cWaitForComplete();
	
	// send device address with read
	i2cSendByte( deviceAddr | 0x01 );
	i2cWaitForComplete();
	
	// check if device is present and live
	if( inb(TWSR) == TW_MR_SLA_ACK)
		{
		// accept receive data and ack it
		while(length > 1)
			{
			i2cReceiveByte(TRUE);
			i2cWaitForComplete();
			*data++ = i2cGetReceivedByte();
			// decrement length
			length--;
			}
		// accept receive data and nack it (last-byte signal)
		i2cReceiveByte(FALSE);
		i2cWaitForComplete();
		*data++ = i2cGetReceivedByte();
		}
		else
			{
			// device did not ACK it's address,
			// data will not be transferred
			// return error
			retval = I2C_ERROR_NODEV;
			}
	
	// transmit stop condition
	// leave with TWEA on for slave receiving
	i2cSendStop();
	
	// enable TWI interrupt
	sbi(TWCR, TWIE);
	
	return retval;
	}
/// @}		// I2C 
