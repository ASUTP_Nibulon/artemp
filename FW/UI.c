/** \file
        Firmware for "ARTemp" - Autonomous Registrator of Temperature
        ***
	by Alexx Grishkov AKA GrayCat 
	User Interface: Keyboard, Sound (headers)                     
	\brief User Interface
*/

//    Version 0.1, started 2007-03-03

#include <avr/io.h>
#include <stdlib.h>
#include <compat/deprecated.h>

     /*** Local def's ***/
#include "UI.h"

// **************************************** V A R S : *****************************************
unsigned char KeyStat;
unsigned char AutoRepCnt;                // Counter of consequent Autorepeat actions
unsigned char LastKeyStat;
volatile unsigned char KbCnt=0;    /* General Counter */
KeybStatFlags_t	KeybStatFlags;

/* ++++++++++++++++++++ Get Keyboard Press +++++++++++++++++++++ */

/**
	Called every tick to determine keys' states, performes Debouncing and Autorepeat. 
	Returnes code of just-pressed	key; updates #KeyStat to current state of keys.
*/
unsigned char  KeyPressed(void)
   {
   unsigned char TmpKeyStat;
	char num_wires = USE_WIRES; // 
	char scanner   = HI_WIRE;   // 
	
   KeyStat = 0xFF;     // Init KBStat  
		// ........... Read Hardware status:  ..............
	do
		{	
		KB_DDR   &= ~(KB_WIRES);	// All lines - Inputs
		KB_PORT	|=   KB_WIRES;		// All lines - Pull-Up
		KB_DDR   |= scanner;			// "scan" line - to output
		KB_PORT  &= ~scanner;		// Pull down "scan" line 
		
		num_wires--;					// decrement + delay
		_NOP();
		_NOP();
		_NOP();
		_NOP();
		_NOP();
		KeyStat &= ( KB_NO_USE | scanner | KB_PIN); // Read pulled-down lines, mask with unused
		scanner  >>= 1;				// next
		} while (num_wires);
	KeyStat = ( (~KeyStat) >> 4) & 0x0F;				// Invert

   if ( (LastKeyStat==KeyStat) )
      {                          /****** Check repeating last state *****/
      if (WaitDebounce && (KbCnt > DebounceTicks))
         {          /*** Finish Debounce wait ***/
         WaitDebounce=0;
         WaitAutoRepeat=1;    /* Start wait for AutoRepeat */

         KbCnt=0;
         return(KeyStat);        /* OK, This key pressed! */
         };   /*** End of Waiting for debounce ***/

      if (KeyStat && AutoRepeat  && (KbCnt > AutoRepeatTicks))
         {       /* Perform AutoRepeat */         
         KbCnt=0;
         return(KeyStat);        /* OK, This key AutoRepeated! */
         };

      if (KeyStat && WaitAutoRepeat  && (KbCnt > WaitAutoRepTicks))
         {       /* Perform AutoRepeat */
         WaitAutoRepeat=0;   /* Not waiting. */
         AutoRepeat=1;

         KbCnt=0;
         return(KeyStat);        /* OK, This key AutoRepeated! */
         };

      }  /*** End of "if (LastKeyStat==KeyStat)" ***/

      else
         {     /**** KeyBoard Changed !!! ****/
         AutoRepCnt = WaitAutoRepeat = AutoRepeat = 0;      /** NOT autorep. anyway **/
         if (KeyStat)             /* Key Pressed */
            WaitDebounce=1;

         KbCnt=0;
         };    /**** End of KB Changed ****/

   TmpKeyStat = LastKeyStat;     // Store...
   LastKeyStat=KeyStat;     /* Update LastKeyStat */
   KeyStat = TmpKeyStat;          // ... Restore
   return(0);   /**** Nothing changed ****/

   };   /***** End of KeyPressed() ******/
/* ------------------------------------------------------------- */

// #########################################################################
// -------------------------------------------------------------------------
// #########################################################################
