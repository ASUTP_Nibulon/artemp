/*****************************************************************/
/*                 ModBUS over USART interface                   */
/*               ASCII version, with Driver control              */
/*****************************************************************/
/*  v0.3                                     by Alexx Grishkov   */
/*  Code file                                                    */
/*****************************************************************/

#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/delay.h>
#include <compat/deprecated.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include 		"binary_c.h"				// Binary "0B0xxx" consts
#define        XTAL_FREQ       14754KHZ            /* Crystal frequency in MHz */

#include "ModBus.h"          /* ModBUS headers interface  */

// +++++++++++++++++++ Status vars +++++++++++++++++++++*/

uchar MB_SlaveAddr=1;               ///< Address as a Slave
uchar MB_OpAddr;                    ///< Current operating Address
uchar MB_OpFunc;                    ///< Current operating Function
volatile uchar MB_Stage;            ///< Current operating Stage
volatile  mb_Statuses_t		mb_Statuses;///< Bit flags for ModBus Statuses

volatile uchar  MB_Error=MBer_None; ///< No error
volatile uint16_t MB_Delay;        	///< Long delays - in 1ms ticks
volatile uchar  LRC_Acc;            ///< Accumulator for Sliding LRC calculation

// ++++++++++++++++++ Buffer VARs  +++++++++++++++++++++*/
volatile uchar MB_BufIdx;      ///< Buffer Index
uchar MB_BufBytes=0;           ///< Number of bytes in Buffer
volatile uchar MB_RxByte;      ///< Byte Received
uchar MB_Buf[MB_BufSize];      ///< Send/Receive buffer
                              
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
//                             P R O C E D U R E S                                 */
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**  
	Reset ModBus drivers:
*/
void ResetModBus(void)
   {
   // SPEN = 0;               // Disable port
   MB_Wait4Reply=0;
   MB_Replying=0;
   MB_Error = MBer_None;
   MB_RxByte = RCREG;
#ifdef PIC			// Only for PIC cores
   OERR = FERR = 0;				// Clear Error flags
#endif
   MB_Delay = 0;
   ResetRX();
   // SPEN = 1;               // Re-enable port
   };    // ----------- ResetModBus() ---------------------------------------------
// --------------------------------------------------------------------------------

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**  
	MODBUS Init                                                                    
*/
void MODBUS_Init(void)
   {
	uint16_t baud_div;
	
#ifdef PIC			// Only for PIC cores
   SPEN = 0;                      // Temp.disable port
   MB_RxByte = RCREG;             // Clear RX reg
	
      //*** Set Baud rate ***
   SPBRG = DIVIDER;               // Calculated from BaudRate

   TXSTA = 0b00100011 | MASK_SPEED;            // 
         //  ||||x|||
         //  |||| ||+------> TX9D, 9th bit to transfer
         //  |||| |+-------> TRMT: "0"=="TSR Full", "1"=="TSR Empty"
         //  |||| +--------> BRGH: "0"=="Low-Speed", "1"=="High-Speed"
         //  |||+----------> SYNC: "0"=="Async. mode"
         //  ||+-----------> TXEN: "1"=="TX Enable"
         //  |+------------> TX9 : "1"=="Enable 9-bit Transmission"
         //  +-------------> CSRC: "0"=="Slave mode" in sync. mode

   RCSTA = 0b10010000;            // USART Enabled, 8-bit, Cont. receive, NO Addr.detect,
         //  ||||||||
         //  |||||||+------> RX9D: 9th received bit
         //  ||||||+-------> OERR: "1"=="Overrun Error"
         //  |||||+--------> FERR: "1"=="Framing Error"
         //  ||||+---------> ADDEN: "0"=="Disable address detect"
         //  |||+----------> CREN: "1"=="Enable [Continuous] receive"
         //  ||+-----------> SREN: "0"=="Disable sync. receive" in Sync. mode
         //  |+------------> RX9:  "1"=="Enable 9-bit Receive"
         //  +-------------> SPEN: "1"=="Enable Serial port"

   TXIE = 0;                      // Disable TX interrupt
   RCIE = 1;                      // Enable RC interrupt
#endif

#ifdef __AVR_ARCH__		// Only for AVR architecture
	MB_RxByte = UDR1;             // Clear RX reg
		// ............ Calculate & set Baud Rate: .........
	baud_div = ((F_CPU+(BAUD*8L))/(BAUD*16L)-1);
	UBRR1H = baud_div>>8;
	UBRR1L = baud_div;
	
		// ............. Init USART ........................
//	UCSR1A = _0b00100000;
		//	      ||||||||
		//	      |||||||+--->	MPCMn: Multi-Processor Communication Mode
		//	      ||||||+---->	U2Xn: Double the USART Transmission Speed
		//	      |||||+----->	UPEn: Parity Error
		//	      ||||+------>	DORn: Data OverRun
		//	      |||+------->	FEn: Frame Error
		//	      ||+-------->	UDREn: USART Data Register Empty
		//	      |+--------->	TXCn: USART Transmit Complete
		//	      +---------->	RXCn: USART Receive Complete
	
	UCSR1B = _0b11011000;
		//	      ||||||||
		//	      |||||||+--->	TXB8n: Transmit Data Bit 8
		//	      ||||||+---->	RXB8n: Receive Data Bit 8
		//	      |||||+----->	UCSZn2: Character Size
		//	      ||||+------>	TXENn: Transmitter Enable
		//	      |||+------->	RXENn: Receiver Enable
		//	      ||+-------->	UDRIEn: USART Data Register Empty Interrupt Enable
		//	      |+--------->	TXCIEn: TX Complete Interrupt Enable
		//	      +---------->	RXCIEn: RX Complete Interrupt Enable
	
	UCSR1C = _0b00000110;
		//	      ||||||||
		//	      X||||||+--->	UCPOLn: Clock Polarity
		//	       ||||++---->	UCSZn1:0: Character Size: "11"=8bit
		//	       |||+------>	USBSn: Stop Bit Select: "0"=1bit, "1"=2bit
		//	       |++------->	UPMn1:0: Parity Mode: "00"=Disabled
		//	       +--------->	UMSELn: USART Mode Select: "0"=Async, "1"=Sync
#endif	//  __AVR_ARCH__		// Only for AVR architecture

#ifdef Use_RS485
   USART_EN_TRIS = 0;             // Allow USART_EN for Output
   USART_EN = USART_en_RX;        // Enable RX driver

#endif	// Use_RS485
   DDRD &= ~(1 << 2);
	ResetModBus();
  
   };  /******* End of MODBUS_Init() *****/
/* ------------------------------------------------------------- */


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/**  
	Send \c NumBytes Data bytes from \c *Buf to \c Addr, function \c FuncNum
	if (Buf==NULL), don't copy it to MB_Buf 
*/
uchar MODBUS_Send(uchar Addr, uchar FuncNum, uchar NumBytes, const uchar *Buf)
   {
      /* Check for Busy status */
   if (MB_Stage)
      {
      MB_Error=MBer_Busy;
      return(MBer_Busy);      //  ERROR: ModBUS is Busy!
      };
   ResetModBus();

      /* Check for Send size */
   if ( NumBytes > (MB_BufSize-3) )
      {
      MB_Error=MBer_TxOverflow;
      return(MBer_TxOverflow);      //  ERROR: Buffer size Overflow!
      };

      /**** Store Operational values ****/
   LRC_Acc = 0;      // Init
   LRC_Acc += (MB_Buf[0]=MB_OpAddr = Addr);    // Update 
   LRC_Acc += (MB_Buf[1]=MB_OpFunc = FuncNum);

      // ...... Copy [Buf] to output buffer .......
   MB_BufBytes = 2;     // Reserve 2 bytes for Addr/FuncCode
   for (MB_BufIdx=0; MB_BufIdx < NumBytes; MB_BufIdx++)  // walk through Input buffer
      {
      if (Buf != NULL)           // Check whether to copy buffer
         MB_Buf[MB_BufBytes]=Buf[MB_BufIdx];
      LRC_Acc += MB_Buf[MB_BufBytes++];     // Update LRC and counter
      };

      // .. Calculate CRC .....
   MB_Buf[MB_BufBytes++] = ((unsigned char)(-((char)LRC_Acc)));
   
   MB_BufIdx = 0;                   // Reset MB_BufIdx
#ifdef Use_RS485
   USART_EN = USART_en_TX;          // Enable TX driver
#endif
   MB_Stage = MBStage_PreGap;       // Starting stage   
   LowNibble = 0;                   // Clear LowNibble for first action  
   MB_Error=MBer_None;
   MB_Delay = MB_TicksByte/**2u*/;      // Time of PreGap stage

   return(0);    // No error
   };      /*----------- End of MODBUS_Master_Send() -----------------*/
/*---------------------------------------------------------------------------------*/

#ifdef UseFn16         // Whether to compile code for function 16
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*  Prepare <NumInts> Integers to send to <RegNum> with Fn 16 in <MB_Buf>          */
uchar MODBUS_PrepFn16(ushort RegNum, uchar NumInts, ...)
   {
   va_list        ap;
   Int2Chars      Int2put;

   va_start(ap, NumInts);

   Int2put.Int = RegNum;
   MB_Buf[2] = Int2put.ch.High;        // Register number:
   MB_Buf[3] = Int2put.ch.Low;         // Register number:

   Int2put.Int = NumInts;
   MB_Buf[4] = Int2put.ch.High;        // Quantity of Registers
   MB_Buf[5] = Int2put.ch.Low;         // Quantity of Registers

   MB_Buf[6] = NumInts*2;              // Quantity of Bytes

   RegNum = 7;                      // Base offset from Start
   while (NumInts--)
      {
      Int2put.Int = va_arg(ap, unsigned int);
      MB_Buf[RegNum++] = Int2put.ch.High;
      MB_Buf[RegNum++] = Int2put.ch.Low;
      };       /*------ End ov Vars loop -----*/

   va_end(ap);       // Finish VarArgList process

   return(RegNum-2);   // Return number of bytes in message

   };     // ------------------ End of MODBUS_PrepFn16() --------------------
// --------------------------------------------------------------------------------
#endif      // UseFn16         // Whether to compile code for function 16

#ifdef UseFn03         // Whether to compile code for function 03
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Parses <NumInts> Integers from <MB_Buf> after request <RegNum> with Fn 03 into vars
uchar MODBUS_ParseFn03(uchar NumPtrs, ...)
   {
   va_list        ap;
   Int2Chars      Int2put;   
   uint16_t       * VarPtr;
   uchar          cnt;

   va_start(ap, NumPtrs);

   MB_Buf[2] /= 2u ;                 // get Quantity of Registers

   for (cnt=0; cnt<NumPtrs; cnt++)
      {
      if (cnt >= MB_Buf[2])   // Prevent reading behind the buffer
         break;
         
      VarPtr = va_arg(ap, unsigned short* );
      Int2put.ch.High = MB_Buf[cnt*2u+3];
      Int2put.ch.Low  = MB_Buf[cnt*2u+4];
      *VarPtr = Int2put.Int;
      };       /*------ End ov Vars loop -----*/

   va_end(ap);       // Finish VarArgList process

   return(cnt);   // Return number of words in message

   };     // ------------------ End of MODBUS_ParseFn03() --------------------
// --------------------------------------------------------------------------------
#endif      // UseFn03         // Whether to compile code for function 03

#ifdef UseFn11         // Whether to compile code for function 11h
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Parses <NumInts> Integers from <MB_Buf> after request <RegNum> with Fn 11 into vars
uchar MODBUS_ParseFn11(uchar NumPtrs, ...)
   {
   va_list        ap;
   Int2Chars      Int2put;   
   ushort       * VarPtr;
   uchar          cnt;

   va_start(ap, NumPtrs);

   MB_Buf[2] / = 2u ;                 // get Quantity of Registers

   for (cnt=0; cnt<NumPtrs; cnt++)
      {
      if (cnt >= MB_Buf[2])   // Prevent reading behind the buffer
         break;
         
      VarPtr = va_arg(ap, unsigned short* );
      Int2put.ch.High = MB_Buf[cnt*2u+3];
      Int2put.ch.Low  = MB_Buf[cnt*2u+4];
      *VarPtr = Int2put.Int;
      };       /*------ End ov Vars loop -----*/

   va_end(ap);       // Finish VarArgList process

   return(cnt);   // Return number of words in message

   };     /*------------------- End of MODBUS_PrepFn11() --------------------*/
/*---------------------------------------------------------------------------------*/
#endif      // UseFn11         // Whether to compile code for function 11

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Reset ModBus Receiver:
void ResetRX(void)
   {
   MB_Delay = MB_Stage=MB_BufIdx=MB_BufBytes=LRC_Acc=LowNibble=MB_RxPending=0;
   };    // --------------- ResetRX() ------------------------------------
// --------------------------------------------------------------------------------

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Convert received 2-char HEX into one binary Byte
/**
	\param InChar - Hexadecimal char (if not, generate Error)
	\return Nibble put to Buffer
*/
void RxChar(uchar InChar)
   {

   if (InChar==':')   // Check if it's Frame Start...
      {     // +++++++++++++ Frame Start +++++++++++++++++
      MB_Error=MBer_None;     // Reset error

      ResetRX();                    // Reset Receiver
      MB_Stage = MBStage_Rx;     // Change Stage - Start Rx
      MB_Delay = MB_TicksSec;     // Maximum delay between bytes in packet
      return;
      };    // -------- End of "Frame Start" -----------

   if ( (InChar==0x0A) && (MB_Stage==MBStage_WaitEnd) )   // Check if it's Frame Finish...
      {     // +++++++++++++ Frame end +++++++++++++++++
      uchar TmpMB_BufBytes;
         // .......... Check LRC:  ..............
      if ( LRC_Acc )      // Total LRC not Zero?...
         {     // ++++++++++++ LRC Error! +++++++++++++++
         MB_Error=MBer_CRC;      // Error,
         goto Reset_Ret;
         };    // ----------- End of "LRC Error!" -------
      TmpMB_BufBytes = MB_BufBytes;    // Store before Reset RX
      ResetRX();                    // Reset Receiver
      MB_BufBytes = TmpMB_BufBytes;    // ReStore after Reset RX
      MB_RxPending = 1;             // Raise flag
      return;
      };    // -------- End of "Frame end" -----------

      // ========== Normally, here we can get ONLY in "MBStage_Rx" stage   =========
   if (MB_Stage!=MBStage_Rx)       // Check stage
      {        // ++++++++++++ Invalid stage for that point! ++++++++++
      MB_Error=MBer_FrameIncomp;      // Error,
      goto Reset_Ret;
      };       // --------- End of "Invalid stage for that point" -------------

   if ( (InChar==0x0D) && (MB_Stage==MBStage_Rx) )   // Check if it's Frame End...
      {     // +++++++++++++ Frame end +++++++++++++++++
      MB_Stage = MBStage_WaitEnd;
      MB_Delay = MB_TicksSec;     // Maximum delay between bytes in packet
      return;
      };    // -------- End of "Frame end" -----------

   InChar -= '0';      // make a Dec Num,
   if (InChar > 9u )         // Not a Dec Num?...
      {     // +++++++++ Not a Dec Num ++++++++++++
      if (InChar >= 17u )         // HEX letters?...
         InChar -= 7;      // make a Bin Num,
         else  // InChar was Between "9" and "A"
            goto Invalid_char;   //
      };    // -------- Not a Dec Num ---------

   if ( InChar>0x0Fu )   // Check if it's not a Number...
      {     // ++++++++++++++ Invalid char -- too small ++++++++++++++
Invalid_char:
      MB_Error=MBer_InvalChar;      // Error,
Reset_Ret:
      ResetRX();                    // Reset Receiver
      return;
      };    // ------------ End of "Invalid char -- too small" -------

      // ...... Check Buffer Overrun:   .........
   if (MB_BufIdx>=MB_BufSize)
      {     // ++++++++++++++++ Buffer Overrun: ++++++++++++++++++
      MB_Error=MBer_RxOverflow;
      goto Reset_Ret;
      };    // --------- End of "Buffer Overrun" -----------------

   MB_Stage = MBStage_Rx;     // Change Stage - Start Rx
   MB_Delay = MB_TicksSec;     // Maximum delay between bytes in packet

      // .... OK, Now, "InChar" is binary, right-aligned
   if (LowNibble)         // Now working on Low Nibble?...
      {     // +++++++++++ Now working on Low Nibble! +++++++++++++++++++++
      LRC_Acc += (MB_Buf[MB_BufIdx++] |= InChar);     // Place new nibble to Buffer, advance AND calculate LRC
      MB_BufBytes++;    // Update counter
      }     // -------- End of "Now working on Low Nibble" ----------------
      else
         {        // ++++++++++ Now working on High Nibble ++++++++++++++++++++
#ifdef PIC
// for PIC18, this changed to next line:       InChar = InChar;     // Select bank...
         FSR2L=FSR2L;
// for PIC18, this changed to next line:       asm("swapf  _RxChar$InChar,F");      // Exchange nibbles
         asm("swapf  _FSR2L,F,c");      // Exchange nibbles
#else
			InChar <<= 4;						// Make nibble High
#endif
         MB_Buf[MB_BufIdx] = InChar;     // Place new nibble to Buffer
         };       // --------- End of "Now working on High Nibble" ------------

   LowNibble ^= 1;    // Toggle LowNibble for next action

   };    // ---------------- End of "RxChar()" ------------------------------------
// --------------------------------------------------------------------------------

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Convert one binary Byte into 2-char HEX for Transmit
/**
	\param  [Next Char in Buffer]
	\return Hexadecimal char
*/
uchar TxChar(void)
   {
   uchar InChar;     // Temporary var

   InChar = MB_Buf[MB_BufIdx];      // Get Next byte

      // .... OK, Now, "InChar" is binary, right-aligned
   if (LowNibble)         // Now working on Low Nibble?...
      {     // +++++++++++ Now working on Low Nibble! +++++++++++++++++++++
      MB_BufIdx++;    // Update counter
      }     // -------- End of "Now working on Low Nibble" ----------------
      else
         {        // ++++++++++ Now working on High Nibble ++++++++++++++++++++
#ifdef PIC
// for PIC18, this changed to next line:        InChar = InChar;     // Switch bits
// for PIC18, this changed to next line:        asm("swapf  _TxChar$InChar,F");      // Exchange nibbles
         asm("swapf  _FSR2L,F,c");      // Exchange nibbles
#else
			InChar >>= 4;
#endif
         };       // --------- End of "Now working on High Nibble" ------------

   InChar &= 0x0F;     // leave only 4 bits
   LowNibble ^= 1;    // Toggle LowNibble for next action

   InChar += '0';      // make a Bin Num,
   if (InChar > '9' )       // HEX figures?...
      InChar += 7;      // make a Bin Num,

   return(InChar);

   };    // ---------------- End of "TxChar()" ------------------------------------
// --------------------------------------------------------------------------------


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/**
	Check Received Packet.
	\return #MBrx_None if no pending RX Packet;                                              
*/
MBrx_RetVals_t MODBUS_Receive(void)
   {
   Int2Chars RecvCRC;
   uchar rcnt;     // Register counter
   uint16_t RegNum;

   if (!MB_RxPending)
      return(MBrx_None);
      
   MB_RxPending = 0;
   
   if (MB_Error)     // ANY error?...
      {     // ++++++++ ANY error ++++++++
      ResetModBus();
      return(MBrx_None);
      };    // --------- ANY error ---------
		
	if ( (MB_Buf[0]!=MB_SlaveAddr)  && MB_Buf[0] )
		{     // ++++++++ ANY error ++++++++
      ResetModBus();
      return(MBrx_None);
      };    // --------- ANY error ---------

   switch (MB_Buf[1])      /*********** Action depending upon FuncNum ********/
      {
      case MBF_Force1Reg:     //+++++++++++++++++++ Function: Force Single Register
               RecvCRC.ch.High = MB_Buf[2];
               RecvCRC.ch.Low  = MB_Buf[3];  // => RecvCRC.Int == RegNum

               for (rcnt=0; rcnt<MBvarsQuant; rcnt++)   // Scan Registers
                  {
                  if (RecvCRC.Int==MB_Vars[rcnt].RegNum)
                     {           /*+++++++++ OK, found appropriate reg. +++++++*/
                     RecvCRC.ch.High = MB_Buf[4];
                     RecvCRC.ch.Low  = MB_Buf[5];  // => RecvCRC.Int == RegNum
                     *((int *)MB_Vars[rcnt].VarPtr) = RecvCRC.Int;
                     goto Fn6_OK;      // OK, send reply==quiery
                     };    //----- End of appropriate reg.
                  };   //------- End of Registers Scan
                  // ==> No RegNum match! - Error!
               MB_Error=MBer_InvReg;
                  /** Form NACK Packet **/
               MB_Buf[1] |= 0x80;           // Set high byte of FuncNum
               MB_Buf[2] = MBSer_DataAddr;   // Error code
               MB_BufBytes = 5;
Fn6_OK:
               return(MBrx_SetCommand);
//               break;        // -------------- End of case MBF_Force1Reg -----------------------

      case MBF_ReadMultiIReg:     // Function: Read Multiple Input Registers
      case MBF_ReadMultiHReg:     // Function: Read Multiple Input Registers
               RecvCRC.ch.High = MB_Buf[2];
               RecvCRC.ch.Low  = MB_Buf[3];  // => RecvCRC.Int == RegNum
               RegNum = RecvCRC.Int;         // Remember RegNum
               MB_BufBytes=MB_Buf[5];        // Requested registers quantity
               MB_BufIdx=3;                  // Starting offset for returned data

               while ( MB_BufBytes--)     // Requested registers quantity loop
                  {
                  for (rcnt=0; rcnt<MBvarsQuant; rcnt++)   // Scan Registers
                     {
                     if ( RegNum == MB_Vars[rcnt].RegNum )
                        {           /*+++++++++ OK, found appropriate reg. +++++++*/
                        RecvCRC.Int = *((int *)MB_Vars[rcnt].VarPtr);
                        MB_Buf[MB_BufIdx++] = RecvCRC.ch.High;
                        MB_Buf[MB_BufIdx++] = RecvCRC.ch.Low;  // => RegNum == RecvCRC.Int
                        goto Fn4_OK;      // OK, continue loop
                        };    //----- End of appropriate reg.
                     };   //------- End of Registers Scan

                                    // ==> No RegNum match! - Error!
                  MB_Error=MBer_InvReg;
                     /** Form NACK Packet **/
                  MB_Buf[1] |= 0x80;           // Set high byte of FuncNum
                  MB_Buf[2] = MBSer_DataAddr;   // Error code
                  MB_BufBytes = 5;
                  goto Fn4_Er;  // RegNum Error!
Fn4_OK:
                  RegNum++;
                  };  // ----------End of Requested registers quantity loop

                  /***** Successful completion => form Packet Header ******/
               MB_Buf[2] = MB_BufIdx-3;        // Quantity of bytes to send (in packet)
               MB_BufBytes = MB_Buf[2] +5;     // Quantity of bytes to send (total)
Fn4_Er:
               return(MBrx_GetCommand);
//               break;        // --------------------- End of case MBF_ReadMultiIReg -----------------
		case MBF_RdDataRec:
					return(MBrx_LongGetCmd);
		case MBF_WrDataRec:
					return(MBrx_LongSetCmd);
      };    //---------- End of switch (MB_Buf[1]) ------------

   return(1);
   };     /*------------ End of MODBUS_Receive() -------------*/
/*---------------------------------------------------------------------------------*/
/* ######################################################################### */
