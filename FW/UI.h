/** \file
        Firmware for "ARTemp" - Autonomous Registrator of Temperature
        ***
	by Alexx Grishkov AKA GrayCat 
	User Interface: Keyboard, Sound (headers)                     
	\brief User Interface
*/
/*****************************************************************/

//    Version 0.1, started 2007-03-03

#ifndef	UI_h				// Avoid doubling

// **************************************** Defines: ******************************************

                           //  Delays
#define DebounceTicks    10u    ///< *1 = 10  ms  : System ticks to wait for Debounce
#define AutoRepeatTicks  160u   ///< *1 = 160 ms  : System ticks to pause between AutoRepeat
#define WaitAutoRepTicks 250u   ///< *1 = 250 ms  : System ticks to wait before AutoRepeat

#define LongAutoRepeatLim 15u   ///< Number of AutoRepeats to speed up

   // ************************************* KeyBoard: ******************************
#define KB_PORT		PORTB
#define KB_DDR			DDRB
#define KB_PIN			PINB
#define KB_WIRES	( (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7) )
#define KB_NO_USE	( (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3) )
#define USE_WIRES       4			///< Number of Used Wires in port
#define NO_USE_WIRES		4			///< Number of Non-Used Wires in port
#define HI_WIRE			(1 << 7)	///< Highest used pin

#define kb_Up      	_0b00000110     ///< "Up" key
#define kb_Down     	_0b00001001     ///< "Down" key
#define kb_Left     	_0b00000101     ///< "Left" key
#define kb_Right     _0b00001010     ///< "Right" key
#define kb_Enter		_0b00000011     ///< "Enter" key
#define kb_Esc			_0b00001100     ///< "Escape" key
#define kb_None      _0b00000000     ///< Nothing pressed!!!

// .......... Hardware defines for AVR:  .............
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)               

/* Assign Pin names */

// **************************************** T Y P E S : ***************************************
typedef struct KeybStatFlags_s					///< Keyboard Status flags
		{
		char WaitDebounce		: 1;					///< Waiting debounce period 
		char WaitAutoRepeat	: 1;          		///< Waiting for Autorepeat 
		char AutoRepeat		: 1;					///< Autorepeating! 
		char LongAutoRepeat	: 1;					///< Autorepeating > LongAutoRepeatLim times! 
		} KeybStatFlags_t;

// **************************************** V A R S : *****************************************

extern KeybStatFlags_t	KeybStatFlags;			///< Keyboard Status flags

#define WaitDebounce		KeybStatFlags.WaitDebounce		///< Waiting debounce period 
#define WaitAutoRepeat	KeybStatFlags.WaitAutoRepeat	///< Waiting for Autorepeat 
#define AutoRepeat		KeybStatFlags.AutoRepeat		///< Autorepeating! 

extern unsigned char KeyStat;					///< Current mask of pressed keys
extern unsigned char AutoRepCnt;     			///< Counter of consequent Autorepeat actions
extern unsigned char LastKeyStat;				///< Previous mask of pressed keys
extern volatile unsigned char KbCnt;    	///< Ticks Counter for pending keys' state

/********************************************** Procedures *********************************/

unsigned char KeyPressed(void);					///< Determine state of Keys

// ------------------------------------------------------------------------------------------
//                                         # # # # # #
// ------------------------------------------------------------------------------------------
#define	UI_h				///< Avoid doubling
#endif	//	UI_h				// Avoid doubling
