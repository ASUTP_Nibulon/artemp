/** \file
        Firmware for "ARTemp" - Autonomous Registrator of Temperature.
		  Headers.
        ***
        v0.2 by GrayCat
        v0.2 :  01-03-2007
	\brief Main Defines file of Firmware 
*/

#ifndef ARTemp_h			// Prevent doubling

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                         Global defines:                          ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// \defgroup Logic_defines Logical defines
/// @{
#define NumChannels           8                     	///< Quantity of ThermoChannels
#define kol_dat 					30                      ///< Quantity of Sensors per Channel
#define Power_TimeOut			120u							///< Timeout for Auto-Power-Off, in half-seconds
#define MaxErrNum 				4u                      ///< Maximum number of erroneous answers

#define lcd_QueueLen				32u							///< Queue length for LCD:
#define MSEC						*2								///< Macro for Millisecond according to OS
/// @}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                       Physical defines:                          ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// \defgroup Physical_defines Physical defines
/// @{
#define ds_OutPort				PORTC							///< Port for Dallas Outputs
#define ds_InPort					PINA							///< Port for Dallas Inputs
	// .............. Power:  .......................
#define PowerSw_ON				PORTB |= 1					///< Macro to switch power ON
#define PowerSw_OFF				PORTB &= ~1					///< Macro to switch power OFF
#define PowerPin					(PINB & 1)					///< Macro to Sense power Pin
#define PowerPinIn				{DDRB&=~1; PORTB &=~1;}	///< Macro to switch Power pin to Input
#define PowerPinOut				{ PORTB |=1; DDRB|= 1; }	///< Macro to switch Power pin to Output

#define ChargingState			(PINE & _BV( 7 ) ) 		///< State of Battery Charging
#define ChrgSensePullUp			{ DDRE|=0x40;PORTE|=0x40; }		///< Pull Up Charge Sensor
#define ChrgSenseRelease		DDRE&=~0x40					///< Release Charge Sensor

// PORTE
#define led_line2					2								///< Number of Bit for Charge/Power LEDs
#define led_lineA					4								///< Number of Bit for "A" LEDs common
#define led_lineB					5								///< Number of Bit for "B" LEDs common
#define LEDs_port					PORTF
/// @}

	// ............... Compatibility: .....................
#define NOP() do { __asm__ __volatile__ ("nop"); } while (0)               
#define WDR() do { __asm__ __volatile__ ("wdr"); } while (0)               
#define SLEEP() do { __asm__ __volatile__ ("sleep"); } while (0)           
#define FALSE 0
#define TRUE 1
// .......... Non-Standard procs:
#define eeprom_rb(addr) eeprom_read_byte ((uint8_t *)(addr))
#define eeprom_rw(addr) eeprom_read_word ((uint16_t *)(addr))
#define eeprom_wb(addr,val) eeprom_write_byte ((uint8_t *)(addr), (uint8_t)(val))

/// \defgroup I2C I2C API
/// @{
// TWSR values (not bits)
// (taken from avr-libc twi.h - thank you Marek Michalkiewicz)
// Master
#define TW_START                                        0x08
#define TW_REP_START                            0x10
// Master Transmitter
#define TW_MT_SLA_ACK                           0x18
#define TW_MT_SLA_NACK                          0x20
#define TW_MT_DATA_ACK                          0x28
#define TW_MT_DATA_NACK                         0x30
#define TW_MT_ARB_LOST                          0x38
// Master Receiver
#define TW_MR_ARB_LOST                          0x38
#define TW_MR_SLA_ACK                           0x40
#define TW_MR_SLA_NACK                          0x48
#define TW_MR_DATA_ACK                          0x50
#define TW_MR_DATA_NACK                         0x58
// Slave Transmitter
#define TW_ST_SLA_ACK                           0xA8
#define TW_ST_ARB_LOST_SLA_ACK          0xB0
#define TW_ST_DATA_ACK                          0xB8
#define TW_ST_DATA_NACK                         0xC0
#define TW_ST_LAST_DATA                         0xC8
// Slave Receiver
#define TW_SR_SLA_ACK                           0x60
#define TW_SR_ARB_LOST_SLA_ACK          0x68
#define TW_SR_GCALL_ACK                         0x70
#define TW_SR_ARB_LOST_GCALL_ACK        0x78
#define TW_SR_DATA_ACK                          0x80
#define TW_SR_DATA_NACK                         0x88
#define TW_SR_GCALL_DATA_ACK            0x90
#define TW_SR_GCALL_DATA_NACK           0x98
#define TW_SR_STOP                                      0xA0
// Misc
#define TW_NO_INFO                                      0xF8
#define TW_BUS_ERROR                            0x00

// defines and constants
#define TWCR_CMD_MASK           0x0F
#define TWSR_STATUS_MASK        0xF8

// return values
#define I2C_OK                          0x00
#define I2C_ERROR_NODEV         0x01

/// I2C Interface States:
typedef enum
		{
		I2C_IDLE = 0, I2C_BUSY = 1,
		I2C_MASTER_TX = 2, I2C_MASTER_RX = 3,
		I2C_SLAVE_TX = 4, I2C_SLAVE_RX = 5
		} eI2cStateType;
// Low-level I2C transaction commands

void i2cSendStart(void);	///< Send an I2C start condition in Master mode
void i2cSendStop(void);	///< Send an I2C stop condition in Master mode
void i2cWaitForComplete(void);	///< Wait for current I2C operation to complete
void i2cSendByte(u08 data);	///< Send an (address|R/W) combination or a data byte over I2C
void i2cReceiveByte(u08 ackFlag);	///< Receive a data byte over I2C
u08 i2cGetReceivedByte(void);		///< Pick up the data that was received with i2cReceiveByte()
u08 i2cGetStatus(void);		///< Get current I2c bus status from TWSR

// high-level I2C transaction commands
u08 i2cMasterSendNI(u08 deviceAddr, u08 length, u08* data);		///< send I2C data to a device on the bus (non-interrupt based)
u08 i2cMasterReceiveNI(u08 deviceAddr, u08 length, u08 *data);		///< receive I2C data from a device on the bus (non-interrupt based)
void i2cMasterSend(u08 deviceAddr, u08 length, u08 *data);			///< send I2C data to a device on the bus
void i2cMasterReceive(u08 deviceAddr, u08 length);		///< receive I2C data from a device on the bus

eI2cStateType i2cGetState(void);		///< Get the current high-level state of the I2C interface

// I2C state and address variables
extern volatile eI2cStateType I2cState;
extern u08 I2cDeviceAddrRW;
// send/transmit buffer (outgoing data)
#define I2C_SEND_DATA_BUFFER_SIZE	32u
extern u08 I2cSendData[I2C_SEND_DATA_BUFFER_SIZE];
extern u08 I2cSendDataIndex;
extern u08 I2cSendDataLength;
// receive buffer (incoming data)
#define I2C_RECEIVE_DATA_BUFFER_SIZE	32u
extern u08 I2cReceiveData[I2C_RECEIVE_DATA_BUFFER_SIZE];
extern u08 I2cReceiveDataIndex;
extern u08 I2cReceiveDataLength;

/// @}		// I2C

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                       C O N S T A N T S  :                       ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// \defgroup gTaskFlags Task flags for OS
/// @{
#define tf_RxPacket			0x0001u		///< Flag Received Packet!

/// @}		// gTaskFlags

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++                            T Y P E S :                           ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Type of combined Signed/Unsigned INT and 2*CHAR
typedef union Int2Chars_u
        {
        uint16_t	uint;				///< Unsigned 16-bit Temperature
		  int16_t 	sint;				///< Signed 16-bit Temperature
        unsigned char chr[2];	///< Separate bytes
        } Int2Chars_t;

/// States of Battery: 
typedef enum BattState_e
		{
		bst_Discharged,				///< Discharged, BAD
		bst_Normal,						///< Normal Battery job
		bst_Charging,					///< Battery Charging
		bst_Charged,					///< Battery Charged completely
		} BattState_t;
		
/// Commands to  Dallas process:
typedef enum ds_Message_e
		{
		dsmsg_Reset,					///< Send Reset to all Dallases
		dsmsg_SearchROMs,				///< Search ROMs present
		dsmsg_ReadTemps,				///< Convert & read Temperatures
		} ds_Message_t;
		
/// Main Dallas Descriptor
typedef struct Dallas_s
		{
		
		} Dallas_t;

/// EEPROM Record Types:
typedef enum eep_RecType_e
		{
		rec_Unform0,		///< 0x00, 0xFF	“Unformat”	Неформатированные области, переводятся в тип “Free”
		rec_Root,			///< 0x01	“Root”	Корневая запись, указывает начало цепочек “Depot”, “Free”, “Note”
		rec_Depot,			///< 0x02	“Depot”	Описывает хранилище, указывает начало цепочки “THang”
		rec_THang,			///< 0x03	“THang”	Описывает термоподвеску, указывает начало цепочки “Dallas”
		rec_Dallas,			///< 0x04	“Dallas”	Описывает термодатчик, содержит адрес и последнюю температуру
		rec_Note,			///< 0x05	“Note”	Текстовые описания объекта
		rec_Free,			///< 0x06	“Free”	Свободные блоки
		rec_Unform1=0xFF,	///< 0x00, 0xFF	“Unformat”	Неформатированные области, переводятся в тип “Free”
		} eep_RecType_t;

/// General Structure of EEPROM Record:
typedef struct eep_GenRecord_s
		{
		eep_RecType_t		RecType;		///< Type of Record
		uint16_t				NextIdx;		///< Index of Next Record of peer type
		unsigned char		Data[11];	///< Record Data
		uint16_t				FirstIdx;	///< Index of First Record of Child type
		} eep_GenRecord_t;

/// Structured Buffer for single Record:
typedef union eep_Record_u
		{
		uint8_t				Buffer[16];				///< Buffer for single Record
		eep_GenRecord_t	GenRecord;				///< General Record type
		} eep_Record_t;

/// Structured Buffer for Real-Time Clock data:
typedef struct rtc_Data_s
		{
		uint8_t 				Seconds;					///< Real-Time Seconds;
		uint8_t 				Minutes;					///< Real-Time Minutes;
		uint8_t 				Hours;					///< Real-Time Hours
		uint8_t 				Day;						///< Real-Time Day-of-Week
		uint8_t 				Date;						///< Real-Time Date of Month
		uint8_t 				Month;					///< Real-Time Month
		uint8_t 				Year;						///< Real-Time Year
		} rtc_Data_t;

// ==========================================================================
// ======                      V A R I A B L E S :
// ==========================================================================

extern unsigned short		PowerOffTO;						///< PowerOff TimeOut
extern BattState_t				BattState;					///< Battery state
extern unsigned char			USBpower;					///< State of Power Source

// JJJJJJJJJJJJJJJJJJJJJJJJJJJ   JacOS related:      JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ
   
   // ........ Flag vars:  ..................
extern OST_FLAGS   TaskFlags;									///< Individual tasks flags, see #gTaskFlags constants

	// ............. Message Queues:  .........
extern OST_MSGQUEUE			lcd_msgq;						///< JacOS Message Queue variable
extern OST_MSGQ_DESCR		lcd_mqdscr;						///< JacOS Message Queue Descriptor
extern OST_MSG_P				lcd_mbuff[lcd_QueueLen];	///< JacOS Message Queue Buffer


// ==========================================================================
// ======                      F U N C T I O N S :
// ==========================================================================

void PutCh_queue(unsigned char Ch);	///< Put a character into OS message queue:
void MainInit(void);						///< Main initialization tasks:

// ==========================================================================
#define ARTemp_h			///< Prevent doubling
#endif 						// ARTemp_h			// Prevent doubling
// --------------------------------------------------------------------------
