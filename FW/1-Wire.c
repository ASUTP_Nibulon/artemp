/** \file
	Dallas 1-Wire protocol low-level routines
	***
	by GrayCat.
	\brief Dallas 1-Wire protocol low-level routines
*/
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>

#include <avr/delay.h>
#include <compat/deprecated.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "binary_c.h"				// Binary "0B0xxx" consts
#include "1-Wire.h"             	// Local


// ==========================================================================
// ======                      F U N C T I O N S :
// ==========================================================================
