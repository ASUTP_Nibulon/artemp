/** \file
                 ModBUS over USART interface                   
	***
  v0.4,                                      by Alexx Grishkov   
               ASCII version, with Driver control.
  Headers file                                                 
	\brief ModBUS over USART interface
*/

#ifndef MODBUS_h    // Protect from doubling

typedef unsigned char	uchar;		///< compatibility "Byte" type

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                          Physical Definition                             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
// #define BAUD  2400uL
// #define BAUD  9600
// #define BAUD  19200uL
// #define BAUD  38400uL
// #define BAUD  57600uL

#define BAUD  115200uL					///< Operating Baud rate
// #define Use_RS485						///< Whether to use RS-485 "Driver Enable" output

#ifdef Use_RS485
	#define USART_EN        RC2			///< USART/RS485_Driver Enable
	#define USART_EN_TRIS   TRISC2   	///< USART/RS485_Driver Enable line Output
	#define USART_en_TX     1u    		///< pin value for TX state (Driver enabled)
	#define USART_en_RX     0u 			///< pin value for RX state (Receiver enabled)
#endif	// Use_RS485

   /********** Defines ************/
#define MB_BufSize      64u    		///< Buffer size, must be >=8

#if BAUD==9600
   #define MB_TicksByte    1u    	///< System Ticks per Byte
   #define MB_TicksSec     1000u 	///< System Ticks per Second
#endif   // 9600
#if BAUD==2400uL
   #define MB_TicksByte    4u    	///< System Ticks per Byte
   #define MB_TicksSec     1000u 	///< System Ticks per Second
#endif   // 9600
#if BAUD==115200
   #define MB_TicksByte    1u    	///< System Ticks per Byte
   #define MB_TicksSec     1000u 	///< System Ticks per Second
#endif   // 9600

#define MaxAddr         100u  		///< Maximum allowed Slave address

#ifdef __AVR_ARCH__		// Only for AVR architecture
	#define RCREG		UDR1				///< Compatibility
#endif	// __AVR_ARCH__		// Only for AVR architecture

// =================== Compile includes:  ====================================
// #define UseFn16         ///< Whether to compile code for function 16
#define UseFn03         ///< Whether to compile code for function 03
// #ifdef UseFn11          ///< Whether to compile code for function 11h

   /**************** ModBUS Functions **************/
#define MBF_Read1Coil        1u            ///< Read Single Coil
#define MBF_Read1Status      2u            ///< Read Single Input Status
#define MBF_ReadMultiHReg    3u            ///< Read Multi Holding Registers
#define MBF_ReadMultiIReg    4u            ///< Read Multi Input Registers
#define MBF_Force1Coil       5u            ///< Force Single Coil
#define MBF_Force1Reg        6u            ///< Force Single Register
#define MBF_ReadException    7u            ///< Read Single Exception Byte
#define MBF_LoopBack         8u            ///< Test/Diagnostics message
                                           //
#define MBF_ForceMultiCoils 15u            ///< Force Multiple Coils
#define MBF_ForceMultiReg   16u            ///< Force Multiple Registers
#define MBF_ReportSlaveID   17u            ///< Report Slave ID

/// \defgroup Proprietary Proprietary ModBUS functions and codes
#define MBF_SetAddr         0x45u          ///< Set new slave Address \ingroup Proprietary
#define MBF_RdDataRec			0x48u			///< READ DATA RECORD \ingroup Proprietary
#define MBF_WrDataRec			0x49u			///< WRITE DATA RECORD \ingroup Proprietary

   /**************** Error Statuses *********/
#define MBer_None          0u             ///< No ModBUS error :-)
#define MBer_Busy          1u             ///< ModBUS is busy now
#define MBer_FrameIncomp   2u             ///< Frame Incomplete
#define MBer_CRC           3u             ///< CRC Error while Receive
#define MBer_InvFunc       4u             ///< Invalid Function Number
#define MBer_InvReg        5u             ///< Invalid Register Number
#define MBer_NoAck         6u             ///< No Acknowledge from Slave
#define MBer_Slave         7u             ///< Slave Error
#define MBer_RxOverflow    8u             ///< Receive buffer Overflow
#define MBer_TxOverflow    9u             ///< Transmit buffer Overflow
#define MBer_InvalChar    10u             ///< Invalid [hex] character received

   /****************** Slave Errors **************/
#define MBSer_None         0u             ///< No Slave error :-)
#define MBSer_FuncNum      1u             ///< Non-existent Function code
#define MBSer_DataAddr     2u             ///< Non-existent Data address
#define MBSer_DataVal      3u             ///< Invalid Data value
#define MBSer_SlaveFail    4u             ///< Slave Failure
#define MBSer_Acknowledge  5u             ///< Acknowledgement for long operations
#define MBSer_SlaveBusy    6u             ///< Slave device Busy
   // ........ Dispatcher-specific codes:  .....................
#define MBSer_NoConUSO     100u           ///< No connection to USO \ingroup Proprietary
#define MBSer_NoUSOrep     101u           ///< No USO reply \ingroup Proprietary
#define MBSer_NoPSIrep     102u           ///< No PSI reply \ingroup Proprietary

   /**************** Working Stages ***************/
#define MBStage_Idle       0u             ///< ModBUS idle...
#define MBStage_PreGap     1u             ///< ModBUS 3.5T gap before Send
#define MBStage_Tx         2u             ///< ModBUS Transmit from Buffer
#define MBStage_PostGap    3u             ///< ModBUS 3.5T gap after Send
#define MBStage_WaitACK    4u             ///< ModBUS waiting for Acknowledge
#define MBStage_WaitReply  5u             ///< ModBUS waiting for Reply
#define MBStage_TurnAround 6u             ///< ModBUS waiting TurnAround time after Send

#define MBStage_Rx         7u             ///< ModBUS Receiveing byte
#define MBStage_Wait15     8u             ///< ModBUS Waiting 1.5T
#define MBStage_Wait20     9u             ///< ModBUS Waiting 2T after 1.5T

#define MBStage_Skip      10u             ///< ModBUS Skipping received packet
#define MBStage_WaitEnd   11u             ///< ModBUS Waiting for final "LF" char

/// Receive proc. return values:
typedef enum MBrx_RetVals_e
		{
		MBrx_None,          						///< Nothing received
		MBrx_SetCommand,    						///< "Set"-type command received
		MBrx_GetCommand,    						///< "Get"-type command received
		MBrx_CRCEr,         						///< CRC Error in received packet
		MBrx_ACK,           						///< ACK Packet from Slave
		MBrx_Reply,         						///< Reply Packet from Slave (with data)
		MBrx_WrongRep,      						///< Wrong Reply Packet 
		MBrx_SlaveErr,      						///< Slave returned error
		MBrx_ExtReq,        						///< External Request received
		MBrx_ExtRep,        						///< External Reply received
		MBrx_LongSetCmd,    						///< Long "Set"-type command received
		MBrx_LongGetCmd,    						///< Long "Get"-type command received
		} MBrx_RetVals_t;

/*--------------------------------------------------------------------------*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                            T Y P E S                                     */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


/// Combined type of Word & two Bytes
typedef union
      {
      uint16_t Int;
      struct
         {
         uchar Low;
         uchar High;
         } ch;
      } Int2Chars;    /*----- End of union Int2Chars ------*/

typedef const struct 			///< Struct describing RegNum<->VarPtr match 
      {
      const uint16_t RegNum;		///< MODBUS Register Number
      void *const VarPtr;		///< Pointer to program variable
      } s_MB_Vars;    /*----- end -----*/
		
typedef struct mb_Statuses_s
		{
		unsigned MBs_RxPending		: 1;		///< Received packet pending
		unsigned MBs_Wait4Reply		: 1;		///< Waiting for Slave reply
		unsigned MBs_Replying		: 1; 		///< Replying to Slave
		unsigned MBs_ExtReq			: 1; 		///< "1" == "Receiving External Request"
		unsigned MBs_LowNibble		: 1;		///< "1" == "Now working on Low Nibble"
		} mb_Statuses_t;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                          V A R I A B L E S                               */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

extern uchar               MB_SlaveAddr;		///< Address as a Slave
extern uchar               MB_OpAddr;   		///< Current operating Address
extern uchar               MB_OpFunc; 			///< Current operating Function
extern volatile  uchar     MB_RxByte; 		///< Byte Received
extern volatile  uchar     MB_Stage; 			///< Current operating Stage
extern volatile  uchar     MB_Error;   		///< No error
extern volatile  uint16_t  MB_Delay;  		///< Long delays - in 0.13ms ticks
extern volatile  uchar     LRC_Acc;   		///< Accumulator for Sliding LRC calculation
extern volatile  mb_Statuses_t	mb_Statuses;///< Bit flags for ModBus Statuses
	// Flags:
#define MB_RxPending			mb_Statuses.MBs_RxPending		///< Received packet pending
#define MB_Wait4Reply		mb_Statuses.MBs_Wait4Reply	///< Waiting for Slave reply
#define MB_Replying			mb_Statuses.MBs_Replying 		///< Replying to Slave
#define ExtReq					mb_Statuses.MBs_ExtReq 		///< "1" == "Receiving External Request"

extern volatile  uchar MB_BufIdx;  			///< Buffer Pointer
extern  uchar MB_BufBytes;   						///< Number of bytes in Buffer
extern  uchar MB_Buf[MB_BufSize];  			///< Send/Receive buffer

      /* ! ! ! ! Define these in the application ! ! ! ! */
extern const uchar MBvarsQuant; 				///< Quantity of vars in MB_Vars[]
extern s_MB_Vars MB_Vars[]; 						///< List of program variables accessible by ModBUS
#define LowNibble				mb_Statuses.MBs_LowNibble 	///< "1" == "Now working on Low Nibble"

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
//                             P R O C E D U R E S                                 */
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void ResetModBus(void);  					///< Reset ModBus drivers:
void RxChar(uchar InChar);					///< Convert received 2-char HEX into one binary Byte
uchar TxChar(void);							///< Convert one binary Byte into 2-char HEX for Transmit
void MODBUS_Init(void);					///<  MODBUS Init 

#ifdef UseFn16         // Whether to compile code for function 16
uchar MODBUS_PrepFn16(ushort RegNum, uchar NumInts, ...);	///<  Prepare <NumInts> Integers to send to <RegNum> with Fn 16 in <MB_Buf>
#endif   // UseFn16         // Whether to compile code for function 16

#ifdef UseFn03         // Whether to compile code for function 03
uchar MODBUS_ParseFn03(uchar NumPtrs, ...);  ///< Parses <NumInts> Integers from <MB_Buf> after request <RegNum> with Fn 03 into vars
#endif // UseFn03         // Whether to compile code for function 03

uchar MODBUS_Send(uchar Addr, uchar FuncNum, uchar NumBytes, const uchar *Buf);		///<  Send <NumBytes> from *<Buf> to <Addr>, function <FuncNum> 

MBrx_RetVals_t MODBUS_Receive(void);			///< Returns 0 if no pending RX Packet;

void ResetRX(void);        			///< Reset ModBus Receiver:

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
//                          Timer macro Definition                          */
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

// .........................................................................*/
#define MODBUS_h
#endif   // MODBUS_h
// ##########################################################################*/
